%%
cd 'L:\MasterThesis\DataFiles\NetCDF'
addpath 'L:\MasterThesis\MatLab\Workflow\Import_Data\chromCDF'
addpath L:\MasterThesis\MatLab\Workflow\ROI
%Loading MS trace
fileList01  = dir('*01.CDF');
fileList02  = dir('*02.CDF');
Nsamples=[1:6,8:14];
chromX01    = cell(size(Nsamples,2),1);% vector("list",length(fileList))
[rt01,mz01]   = deal(chromX01);

%Parameters for Region of interest (ROI)
thresh=1;
massaccuracy=8*10^-6;% in ppm
multiple=50;
mzerror=0.9;%800*massaccuracy*multiple;% in Da measured a 800 Da
ScanTime=0.35; %Seconds pr. scan
MinPeakWidth=6; %Seconds
minroi=round(MinPeakWidth/ScanTime);
X=zeros(2020,951,size(Nsamples,2));
I=1;
for (f = Nsamples) % length(fileList))
  f=4;
    fprintf('File %i/%i\n',f,length(fileList01))
    A= cdf2struct(fileList01(f).name,{'intensity_values','mass_values','point_count','scan_acquisition_time'})  ;
    
    %Organizing data for ROI function.
    %peaks is a cell array with nrows= number of scans. In each cell
    %the mz values, mz intensities measured in the scan is organized
    %[mz_Values mz_Intensity]
    
    peaks=cell(A.point_count.size,1);
    k1=1;
    nrows=A.point_count.size;
    for (S = 1:A.point_count.size)
        k2=k1+A.point_count.data(S)-1;
        peaks{S,1}=[A.mass_values.data(k1:k2),A.intensity_values.data(k1:k2)];
        k1 = A.point_count.data(S)+k1;
    end
%     
%     [mzroi,MSroi,roicell]=ROIpeaks(peaks,thresh,mzerror,minroi,nrows,A.scan_acquisition_time.data);
    [chromX01{f},mz01{f}] = binVec2(A.intensity_values.data,A.mass_values.data,A.point_count.data,1,[1 0 .1]);
    
    X(:,:,I)=chromX01{f};
    I=I+1;
end
X=zeros(2020,951,size(Nsamples,2));
size(X)
for k=1:13
 X(:,:,k)=chromX01{Nsamples(k)};
end 
size(X)

%%
%High energy
for (f = 4) % length(fileList))
    
    fprintf('File %i/%i\n',f,length(fileList02))
    A= cdf2struct(fileList02(f).name,{'intensity_values','mass_values','point_count','scan_acquisition_time'})  ;
    
    %Organizing data for ROI function.
    %peaks is a cell array with nrows= number of scans. In each cell
    %the mz values, mz intensities measured in the scan is organized
    %[mz_Values mz_Intensity]
    
%     peaks=cell(A.point_count.size,1);
%     k1=1;
%     nrows=A.point_count.size;
%     for (S = 1:A.point_count.size)
%         k2=k1+A.point_count.data(S)-1;
%         peaks{S,1}=[A.mass_values.data(k1:k2),A.intensity_values.data(k1:k2)];
%         k1 = A.point_count.data(S)+k1;
%     end
%     
%     [mzroi02,MSroi02,roicell02]=ROIpeaks(peaks,thresh,mzerror,minroi,nrows,A.scan_acquisition_time.data);
[chromX02{f},mz02{f}] = binVec2(A.intensity_values.data,A.mass_values.data,A.point_count.data,1,[1 0 .1]);
 
end


% % %%
% % %Augment two matrices
% % time=1:2020;
% % MSroi01=MSroi;
% % mzroi01=mzroi;
% % for (f = 4)
% %     [MSroi_aug,mzroi_aug,time_aug] = MSroiaug(MSroi01,MSroi02,mzroi01,mzroi02,mzerror,thresh,time,time )
% % end
% %%
% %Sum the low and the high energy trace
% start(1)=1;
% for (f =4)
%     final(1)=start(1)+size(time,2)-1;
%     %2-norm of the low energy trace
%     Norm(1)=norm(MSroi_aug(start:final,:),2);
%     
%     %2-norm of the high energy trace
%     start(2)=start(1)+size(time,2);
%     final(2)=start(2)+size(time,2)-1;
%     Norm(2)=norm(MSroi_aug(start:final,:),2);
%     
%     %Sums the two trace weighted by the ratio of their 2-norms
%     MSROI_Sum(:,:,f)=MSroi_aug(start(2):final(2),:).*Norm(1)/Norm(2) + MSroi_aug(start(1):final(1),:);
%     start(1)=start(2)+size(time,2);
% end
% 
% 
% high=plot(sum(MSroi_aug(2021:4040,:).*Norm(1)/Norm(2),2));
% close all
% figure
% low=plot(sum(MSroi_aug(1:2020,:),2));
% hold on
% legend([low,high],{'low','high'},'Fontsize',25)

