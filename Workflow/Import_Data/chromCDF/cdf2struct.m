function [S] = cdf2struct(fileName, Fields)
% Converts CDF files to structure
%
% [S] = cdf2struct(fileName, Fields)
%
% INPUT
% fileName : name of the CDF file
% Fields   : (n � 1) cell with names of the variables to import (case
%                    insensitive and partial names are allowed)
%            if empty, no actual data is extracted. Only information and
%            attributes.
%
% OUTPUT
% S        : structure with one field for each imported variable and the
%            field 'file' reporting the cdf file name and full path, if provided
%            Each field is itself a structure with the following fields
%            (AttName) -> value for the attribute AttName of the variable
%            'type'    -> integer
%            'size'    -> size according to what is saved in the CDF file
%            'id'      -> variable ID in the CDF
%            'data'    -> content of the variable
% 
%            The additional fields are always present (cf. netcdf documentation)
%            'Global_Attributes' -> global attributes
%            'CDF_Dimensions'    -> names and lengths of the dimensions
% 
% Author: Giorgio Tomasi
%         Giorgio.Tomasi@gmail.com
%
% Created      : 04 November, 2009; 11:14
% Last modified: 08 November, 2009; 14:02

% HISTORY
% 0.00.01 04 Nov 09 -> Generated function with blank help

% Constants
persistent DT
if isempty(DT)
   DT = {'NC_BYTE','NC_CHAR','NC_SHORT','NC_INT','NC_FLOAT','NC_DOUBLE'}';
   for i_dt = 1:length(DT),DT{i_dt,2} = netcdf.getConstant(DT{i_dt});end
end

% Check n. of input parameters
narginchk(1,2)

% Check field names
if nargin > 1
    if (isempty(Fields)), Fields = 'all';
    elseif (ischar(Fields)), Fields = validatestring(Fields,{'all','global','info'},mfilename);
    elseif (~(iscellstr(Fields))), error('Invalid field names'), 
    end
else Fields = 'all';
end

% Open file
if (ischar(fileName))
    [pathStr,fileName,Ext] = fileparts(fileName);
    if (~(isempty(Ext) || strcmpi(Ext,'.cdf'))), warning('struct2cdf:badExtension','''cdf'' is the only extension allowed'); end
    fileName = fullfile(pathStr,[fileName,'.cdf']);
    ncid     = netcdf.open(fileName,'nowrite');
elseif (isnumeric(fileName)), ncid = fileName;
else error('cdf2struct:invalidFileName','Invalid fileName parameter: it must be either an id for a netcdf file/group or a file name')
end

[nDims,nVars,nGAtt,unlimid] = netcdf.inq(ncid);

% Empty CDF
if (isequal(nDims,nVars,nGAtt,0) && unlimid == -1)
    warning('cdf2struct:emptyCDF','The cdf file is empty!')
    S = struct('CDF_Dimensions',{});
    if (~isnumeric(fileName)), netcdf.close(ncid); end
    return
end

% Dimension definition
dimname = cell(nDims,1);
dimlen  = NaN(nDims,1);
if (nDims > 0)
    dimIDs  = netcdf.inqDimIDs(ncid)';
    for (i_dim = 1:length(dimIDs)), [dimname{i_dim},dimlen(i_dim)] = netcdf.inqDim(ncid,dimIDs(i_dim)); end
else
    warning('cdf2struct:emptyDims','The CDF file contains no dimension definition')
end
S = struct('CDF_Dimensions',{cat(2,dimname,num2cell(dimlen))});

% Load variables
if (nVars ~= 0)
    
    if (~ischar(Fields) || all(strcmp(Fields,'all') | strcmp(Fields,'info')))
        
        Variables = cell(nVars,1);
        ind_Var   = 0:nVars - 1;
        for i_var = ind_Var, Variables{i_var + 1} = netcdf.inqVar(ncid,i_var); end
        if (nargin > 1 && iscellstr(Fields))
            
            if (numel(Fields) ~= length(Fields)), error('Fields must be a vector'), end
            a = false(nVars,1);
            for (iField = 1:length(Fields))
                b = strncmpi(Fields{iField},Variables,length(Fields{iField}));
                if (~any(b)), warning('cdf2struct:ignoredfields','Field "%s" has no match',Fields{iField}), end 
                a = a | b; 
            end
            if (~any(a)), return; end
            ind_Var = ind_Var(a);
            
        end
        for i_var = ind_Var
            
            [varname, xtype, dimids, numatts] = netcdf.inqVar(ncid,i_var);
            dimids      = ismember(dimIDs,dimids);
            attrib      = cell(numatts,2);
            if numatts
                
                for i_att = 0:numatts - 1
                    [attrib{i_att + 1,1}] = netcdf.inqAttName(ncid,i_var,i_att);
                    attrib{i_att + 1,2}   = netcdf.getAtt(ncid,i_var,attrib{i_att + 1,1});
                end
                S.(varname) = cell2struct(attrib(:,2),attrib(:,1));
                
            end
            S.(varname).id   = i_var;
            S.(varname).type = DT{[DT{:,2}] == xtype,1};
            S.(varname).size = dimlen(dimids);
            S.(varname).dimName = dimname(dimids);
            if (~all(strcmp(Fields,'info'))), S.(varname).data = netcdf.getVar(ncid,i_var); end
            
        end
        
    end
    
else warning('cdf2struct:noVars','The CDF file contains no variables\n')
end

% Global attributes
if nGAtt

   attrib = cell(nGAtt,2);
   for i_att = 0:nGAtt - 1
      [attrib{i_att + 1,1}] = netcdf.inqAttName(ncid,netcdf.getConstant('NC_GLOBAL'),i_att);
      attrib{i_att + 1,2}   = netcdf.getAtt(ncid,netcdf.getConstant('NC_GLOBAL'),attrib{i_att + 1,1});
   end
   S.Global_Attributes = cell2struct(attrib(:,2),attrib(:,1));
   
end
if (~isnumeric(fileName)), netcdf.close(ncid); end