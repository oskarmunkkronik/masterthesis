function [Xbin,Bins,outBins] = binVec(I,ch,pts,W,opt)
% Bin data matrix
%
% [Xbin,Bins,outBins] = binVec(I,ch,W,opt)
%
% INPUT
% X   : Data vector
% ch  : Scalars for the axis in column mode (e.g., chemical shift or retention time)
%       The columns in X corresponding to NaNs in ch will be ignored in the binning
% pts : boundaries for repeating of ch values
% W   : Resolution of the binning (e.g., 0.01 (ppm))
%       if length(W) > 1 -> bin boundaries
% opt : (1) if 1 -> average bins using the number of data points in each bin (default: 0)
%       (2) if 2 -> return average intensity-weighted channel value for the bin
%              1 -> return average unweighted channel value for the bin
%              0 -> return nominal bins according to W (default)
%       (3) fraction of bin size below the bin centre at which the lower bin boundary is placed (default: 0.5)
%           This makes Bins return the centre value for the bins.
%           One could bin at e.g. nominal values between x.9 and (x + 1).9
%           and have 'x' as bin denomination by setting opt(3) to 0.1
% OUTPUT
% Xbin   : Binned matrix
% Bins   : Centre point in of the bins
% outBins: bins for out-of-boundary values when hard bins are provided (i.e. W is a vector)
%          outBins (:,1) -> ch < min(W); outBins(:,2) -> ch > max(W);
%
% NB. Missing values are ignored
%
% Author: Giorgio Tomasi
%         Giorgio.Tomasi@gmail.com
%
%
narginchk(4,5)
OPTDEF = [0 0 0.5];
if (nargin < 5 || isempty(opt)), opt = OPTDEF; end
if (length(opt) < length(OPTDEF)), opt(end + 1:length(OPTDEF)) = OPTDEF(length(opt) + 1:end); end
if (length(ch) ~= length(I)), error('Scalar vector and data matrices are not compatible in size'); end
if (any(isnan(I)) || any(isnan(ch))), error('binVec:noMissing','No missing values are allowed'); end

[wL,wU,Bins] = getBoundaries(W,ch,opt(3));

% Position of scans along the channel vector
pos    = cumsum(pts(:));
pos    = cat(2,cat(1,1,1 + pos(1:end - 1)),pos);

% Initialisations
nScans     = length(pts);
nBins      = length(wL);
Xbin       = zeros(nScans,nBins,'like',I); % Keep single/double class if need be
outBins    = zeros(nScans,2);
doBins     = nargout > 1 && opt(2);
alg        = 2 * opt(2) + opt(1);

% Bin scan-wise;
v = find(pos(:,2) >= pos(:,1));
if (doBins)

    [chCount,Bins] = deal(zeros(nBins,1));
    for (iRow = v')
        [Xbin(iRow,:),d,f] = binTest(pos(iRow,1),pos(iRow,2),Xbin(iRow,:),ch,I,wL,wU,alg);
        Bins(f)            = Bins(f) + d(f);
        chCount            = chCount + f;
    end
    ind       = chCount > 0; 
    if (any(ind)), Bins(ind) = Bins(ind)./chCount(ind); end
    Bins(~ind) = .5 * (wL(~ind) + wU(~ind)); 
    
else
    for (iRow = v'), Xbin(iRow,:) = binTest(pos(iRow,1),pos(iRow,2),Xbin(iRow,:),ch,I,wL,wU,alg); end
end
    
%-----------------------------------------------------------------------------------------------------------------------------------------------------

function [wL,wU,Bins] = getBoundaries(W,ch,opt)
if (length(W) == 1)
    
    if (W <= 0), error('binVec:nonPositive','Bin width must be positive'); end
    if (opt >= 1), error('binVec:outOfBound','Relative bin offset must be less than 1'); 
    elseif (-opt >= W), error('binVec:outOfBound','Absolute bin offset cannot exceed bin width')
    elseif (~opt), opt = 0.5;
    end
    s    = floor(min(ch)/W) * W;
    e    = ceil(max(ch)/W) * W;
    incr = W;
    if (opt > 0), W = (s - opt * incr):incr:(e + (1 - opt) * W);
    else W = s + opt:W:e + opt;
    end
    wL   = W(1:end - 1);
    wU   = W(2:end);
    if (nargout > 1), Bins = s:incr:e; end
    
elseif (isempty(W)), error('binVec:noBins','No bin width/bin definition given');
elseif (numel(W) == length(W))
    
    d       = diff(W);
    if (any(d < 0)),error('binVec:unsortedBins','Bin boundaries vector must be sorted in increasing order'); end
    if (any(d == 0)), error('binVec:emptyBins','Repeated bin boundaries'); end
    if (nargout > 1), Bins = W(1:end - 1) + opt * diff(W); end
    wL    = W(1:end - 1);
    wU    = W(2:end);

elseif (size(W,2) == 2)
    [wL,ord] = sort(W(:,1),'ascend');
    wU       = W(ord,2);
    if (any(wL > wU)), error('binVec:badBins','Lower bin boundary must be smaller/equal to upper boundary'); end
    Bins = 0.5 * (wL + wU);
else
end

%-----------------------------------------------------------------------------------------------------------------------------------------------------

function [a,d,f] = binTest(s,e,a,x,v,wL,wU,alg) % NOTE: make mex files of the called functions
% alg -> 0    basic (fastest)
%        1    mean intensity within bin
%        2/3  calculate bins' mean mass without/with averaging within bin
%        4/5  calculate bins' mean weighted mass without/with averaging within bin  
if (~alg),         a      = basicBin(s,e,a,x,v,wL,wU);                  % No average, predefined bins - Fastest option
elseif (alg == 1), a      = meanBin(s,e,a,x,v,wL,wU);                   % Average intensity within bin
elseif (alg < 4), [a,d,f] = meanGetBin(s,e,a,x,v,wL,wU,alg);            % Bin with/without average and return mean bin mass
else              [a,d,f] = meanGetWeightedBin(s,e,a,x,v,wL,wU,alg);    % Bin with/without average and return mean bin weighted mass
end

%-----------------------------------------------------------------------------------------------------------------------------------------------------

function a = basicBin(s,e,a,x,v,wL,wU)
n = length(wL);
c = 1;
while (c <= n && s <= e)
    if (x(s) > wU(c)), c = c + 1; continue; end
    if (x(s) > wL(c)), a(c) = a(c) + v(s); end
    s = s + 1;
end

%-----------------------------------------------------------------------------------------------------------------------------------------------------

function a = meanBin(s,e,a,x,v,wL,wU)
% Average within predefined bins
n = length(wL);
c = 1;
b = a;
f = false(size(wL));
while (c <= n && s <= e)
    
    if (x(s) > wU(c))
        f(c) = a(c) > 0;
        c    = c + 1; 
        continue; 
    end
    if (x(s) > wL(c))
        a(c) = a(c) + v(s);
        b(c) = b(c) + 1;
    end
    s = s + 1;
    
end
a(f) = a(f)./b(f);

%-----------------------------------------------------------------------------------------------------------------------------------------------------

function [a,d,f] = meanGetBin(s,e,a,x,v,wL,wU,useMean)
n = length(wL);
c = 1;
b = a;
d = a;
f = false(size(wL));
while (c <= n && s <= e)
    
    if (x(s) > wU(c))
        f(c) = a(c) > 0;
        c    = c + 1; 
        continue; 
    end
    if (x(s) > wL(c))
        a(c) = a(c) + v(s);
        b(c) = b(c) + 1;
        d(c) = d(c) + x(s);
    end
    s = s + 1;
    
end
d(f) = d(f)./b(f);
if (useMean > 2), a(f) = a(f)./b(f); end

%-----------------------------------------------------------------------------------------------------------------------------------------------------

function [a,d,f] = meanGetWeightedBin(s,e,a,x,v,wL,wU,useMean)
n = length(wL);
c = 1;
b = a;
d = a;
f = false(size(wL));
if (useMean > 4)
    
    while (c <= n && s <= e)
        
        if (x(s) > wU(c))
            f(c) = a(c) > 0;
            c    = c + 1;
            continue;
        end
        if (x(s) > wL(c))
            a(c) = a(c) + v(s);
            b(c) = b(c) + 1;
            d(c) = d(c) + x(s) * v(s);
        end
        s = s + 1;
        
    end
    
else
    
    while (c <= n && s <= e)
        
        if (x(s) > wU(c))
            f(c) = a(c) > 0;
            c    = c + 1;
            continue;
        end
        if (x(s) > wL(c))
            a(c) = a(c) + v(s);
            d(c) = d(c) + x(s) * v(s);
        end
        s = s + 1;
        
    end
    
end
d(f)  = d(f)./a(f);
d(~f) = .5 * (wL(~f) + wU(~f));
if (useMean > 4), a(f) = a(f)./b(f); end

