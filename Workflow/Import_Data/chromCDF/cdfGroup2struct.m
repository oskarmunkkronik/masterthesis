function [outStruct] = cdfGroup2struct(fileName, groupName, varargin)
% Converts child group from a cdf to structure
%
% [outStrut] = cdfGroup2struct(fileName, groupName, Fields)
% 
% INPUT
% fileName :
% groupName:
% Fields   :
% 
% OUTPUT
% outStrut:
% 
% Author: Giorgio Tomasi
%         giorgio.tomasi@gmail.com
% 
% Created      : 07 September 2013; 17:49
% Check n. of input
narginchk(1,3)

import chromCDF.cdf2struct

% Read the cdf
doClose = false;
if (isempty(groupName))
    outStruct = cdf2struct(fileName,Fields);
    return
elseif (~ischar(groupName)), error('cdf2structGroup:invalidGroup','Invalid group name');
elseif (ischar(fileName))
    
    [pathStr,fileName,Ext] = fileparts(fileName);
    if (~(isempty(Ext) || strcmpi(Ext,'.cdf')))
        warning('struct2cdfGroup:badExtension','''cdf'' is the only extension allowed')
    end
    fileName = fullfile(pathStr,[fileName,'.cdf']);
    if (~exist(fileName,'file')), error('struct2cdfGroup:fileDoesNotExist','The file does not exist on a valid path.')
    else
        ncid = netcdf.open(fileName,'write');
        doClose = true;
    end
    
elseif (isnumeric(fileName)), ncid = fileName;
else
    error('struct2cdfGroup:invalidFileName','fileName must be a file name or a file/group identifier')
end

% Read groups
grpHan = netcdf.inqGrps(ncid);
if (~isempty(grpHan))
    iGrp  = 1;
    while (iGrp <= length(grpHan) && ~strcmp(netcdf.inqGrpName(grpHan(iGrp)),groupName)), iGrp = iGrp + 1; end
    if (iGrp > length(grpHan)), error('cdfGroup2struct:noGroup','The group "%s" does not exist',groupName); end
    grpId = grpHan(iGrp);
else
    
    if (strcmp(netcdf.inqFormat(ncid),'FORMAT_CLASSIC'))
        warning('cdfGroup2struct:classic','Classic netcdf format does not support groups. The "root" group is extracted')
    else warning('cdfGroup2struct:noGroups','There are no groups for the given pointer/file. The "root" group is extracted')
    end
    grpId = ncid;

end

% read the cdf/child
outStruct = cdf2struct(grpId,varargin{:});

if (doClose), netcdf.close(ncid); end
end
