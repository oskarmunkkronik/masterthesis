function [Outcome] = struct2cdf(fileName, S, cdfVersion, Fields, compLevel, overWrite)
% Write structure in a cdf file
%
% [Outcome] = struct2cdf(S, Fields)
%
% INPUT
% fileName  : name of the file (the extension 'cdf' is added automatically if not present)
% S         : structure that is to be saved in a cdf file
% cdfVersion: 'classic' or 'netcdf4'
% Fields    : fields that are saved
% compLevel : a number between 0 (no compression) and 9 (maximum compression)
%
% OUTPUT
% Outcome:
%
% Author: Giorgio Tomasi
%         Giorgio.Tomasi@gmail.com
%
% Created      : 24 August 2013; 00:01
%

% Check n. of input
narginchk(2,6)

if (nargin < 5 || isempty(compLevel)), compLevel = 0; end
if (nargin < 6 || isempty(overWrite)), overWrite = false; end

% Constants in
persistent DT
if isempty(DT)
    DT = {'NC_BYTE','NC_CHAR','NC_SHORT','NC_INT','NC_FLOAT','NC_DOUBLE'}';
    for i_dt = 1:length(DT),DT{i_dt,2} = netcdf.getConstant(DT{i_dt});end
end

% Check structure
if (~isstruct(S)), error('S must be a structure'); end
if (numel(S) > 1), error('Structure arrays are not allowed'); end
if (nargin < 4 || isempty(Fields)), Fields = fieldnames(S); end
S = struct4cdf(S,Fields);
if (ischar(Fields)), Fields = cellstr(Fields); end
if (~iscellstr(Fields)), error('Invalid field names'), end
if (~(ismatrix(Fields) && any(size(Fields) == 1))), error('Invalid field names: it must be a char array or a cell vector'); end
Fields  = setdiff(Fields(:),{'CDF_Dimensions','Global_Attributes'})';
nFields = length(Fields);
Fields  = Fields(isfield(S,Fields));
if (length(Fields) ~= nFields), warning('struct2cdf:invalidFields','Some fields are not present in the structure. The names will be ignored'); end

% Check dimensions and global attributes
missingDim = ~(all(isfield(S,'CDF_Dimensions')) && iscell(S.CDF_Dimensions') && size(S.CDF_Dimensions,2) == 2 && ...
    iscellstr(S.CDF_Dimensions(:,1)) && all(strcmp(cellfun(@class,S.CDF_Dimensions(:,2),'uniformoutput',0),'double')));
if (missingDim), error('The ''CDF_Dimensions'' is missing or invalid'); end
cdfDims     = S.CDF_Dimensions;
S           = rmfield(S,'CDF_Dimensions');
existGlobal = isfield(S,'Global_Attributes') && isstruct(S.Global_Attributes);
if (existGlobal)
    globalAtt = S.Global_Attributes;
    S         = rmfield(S,'Global_Attributes');
end

% Check filename
if (ischar(fileName))
    
    [pathStr,fileName,Ext] = fileparts(fileName);
    if isempty(Ext), Ext = '.cdf';
    elseif ~strcmpi(Ext,'.cdf'), warning('struct2cdf:badExtension','''cdf'' is the only extension allowed')
    end
    fileName = fullfile(pathStr,[fileName,Ext]);
    % Check version
    if (nargin < 3 || isempty(cdfVersion)), cdfVersion = 'classic'; end
    try
        if (overWrite && exist(fileName,'file'))
            ncid = netcdf.open(fileName,'WRITE');
        else
            
            switch upper(cdfVersion)
                case 'CLASSIC'
                    ncid = netcdf.create(fileName,netcdf.getConstant('NOCLOBBER'));
                    
                case 'NETCDF4'
                    ncid = netcdf.create(fileName,bitor(netcdf.getConstant('NOCLOBBER'),netcdf.getConstant('NETCDF4')));
                    
                otherwise
                    error('Version must be either ''classic'' or ''netcdf4''')
                    
            end
            
        end
        
    catch ME
        if ~isempty(regexp(ME.message,'File exists && NC_NOCLOBBER \(NC_EEXIST\)','once'))
            error('The file already exists and cannot be created')
        else error(ME.message)
        end
    end
    
elseif (isnumeric(fileName))
    ncid     = fileName;
    tempForm = regexprep(netcdf.inqFormat(fileName),'^FORMAT_','');
    if (nargin > 3 && ~(isempty(cdfVersion) || strcmpi(cdfVersion,tempForm))), error('File format does not match input'); end
end

try
    
    % Create the dimensions
    if (~isempty(cdfDims))
        dimid    = NaN(size(cdfDims,1),1);
        for iDim = 1:size(cdfDims,1), dimid(iDim) = netcdf.defDim(ncid,cdfDims{iDim,:}); end
        cdfDims = cat(2,cdfDims,num2cell(dimid));
    end
    
    % Write fields
    varid = NaN(length(Fields),1);
    for (iField = 1:length(Fields))
        
        dimVar = NaN(length(S.(Fields{iField}).size),1);
        for (iDim = 1:length(S.(Fields{iField}).size)), dimVar(iDim) = cdfDims{strcmp(cdfDims,S.(Fields{iField}).dimName{iDim}),3}; end
        varid(iField) = netcdf.defVar(ncid,Fields{iField},S.(Fields{iField}).type,dimVar);
        if (strcmpi(cdfVersion,'netcdf4')), netcdf.defVarDeflate(ncid,varid(iField),true,true,compLevel); end
        varAtt = setdiff(fieldnames(S.(Fields{iField})),{'size','dimName','id','type','data'}');
        if (~isempty(varAtt))
            for (iAtt = 1:length(varAtt)), netcdf.putAtt(ncid,varid(iField),varAtt{iAtt},S.(Fields{iField}).(varAtt{iAtt})); end
        end
        
    end
    if (existGlobal)
        
        if (~isstruct(globalAtt)), error('The "Global_Attributes" field must be a structure'); end
        gAttNames = fieldnames(globalAtt);
        for (iAtt = 1:length(gAttNames))
            netcdf.putAtt(ncid,netcdf.getConstant('NC_GLOBAL'),gAttNames{iAtt},globalAtt.(gAttNames{iAtt}))
        end
        
    end
    netcdf.endDef(ncid)
    for (iField = 1:length(Fields))
        if (~isempty(S.(Fields{iField}).data)), netcdf.putVar(ncid,varid(iField),S.(Fields{iField}).data); end
    end
    if (~isnumeric(fileName)), netcdf.close(ncid); end
    
    if nargout > 0, Outcome = true; end
    
catch ME
    if (~isnumeric(fileName)), netcdf.close(ncid); end
    fprintf('Error: %s\n',ME.message)
    if nargout > 0, Outcome = false; end
end

end

% ----------------------------------------------------------------------------------------------------------------------

function [outStruct] = struct4cdf(inStruct, fieldNames)
% Converts a structure in one that can be used for struct2cdf
%
% [outStruct] = struct4cdf(inStruct)
%
% INPUT
% inStruct   : structure to write in a cdf file
% fieldaNames: fields to be written into variables (empty: retain all)
%
% OUTPUT
% outStruct  : output structure with the necessary fields
%
% Author: Giorgio Tomasi
%         Giorgio.Tomasi@gmail.com
%
% Created      : 27 August 2013; 18:56
% HISTORY
% 0.00.01 27 Aug 13 -> Generated function with blank help

% Check n. of input
narginchk(1,2)
if (~isstruct(inStruct)), error('struct2cdf:invalidInput','Input must be a structure'); end

%% Some initialisations
fN = fieldnames(inStruct);
if (nargin < 2), fieldNames = fN;
else
    if (ischar(fieldNames)), fieldNames = cellstr(fieldNames); end
    if (~iscellstr(fieldNames)), error('struct4cdf:invalidFieldNames','Invalid field names'); end
end
if (~isempty(fieldNames)), outStruct  = rmfield(inStruct,setdiff(fN,fieldNames));
else outStruct = inStruct;
end
if (isfield(outStruct,'Global_Attributes'))
    globAtt   = outStruct.Global_Attributes;
    outStruct = rmfield(outStruct,'Global_Attributes');
else globAtt  = [];
end
if (isfield(outStruct,'CDF_Dimensions')),
    
    CDF_Dimensions = outStruct.CDF_Dimensions;
    outStruct      = rmfield(outStruct,'CDF_Dimensions');
    dimClass       = repmat({'indeterminate'},[size(CDF_Dimensions,1),1]);
    dimClass(~cellfun(@isempty,regexp(CDF_Dimensions(:,1),'_\d+_byte_string'))) = {'char'};
    CDF_Dimensions = cat(2,CDF_Dimensions,dimClass);
    
else CDF_Dimensions = cell(0,3);
end
if (~isempty(CDF_Dimensions))
    if (~iscellstr(CDF_Dimensions(:,1))), error('struct4cdf:invalidCDFdims','The first column in CDF_Dimensions must be a cell vector of strings'); end
    if (~all(cellfun(@(a) isnumeric(a) && (fix(a) == a),CDF_Dimensions(:,2)))), error('struct4cdf:invalidCDFdims','The second column in CDF_Dimensions must be an integer'); end
end
fieldNames = fieldnames(outStruct);

%% Extract class and size of variables
outCell   = structfun(@class,outStruct,'uniformoutput',0);
outCell   = struct2cell(outCell);

% Remove cells
cellInd = find(strcmp(outCell(:,1),'cell'));
if (~isempty(cellInd))
    warning('struct4cdf:cellField','Fields containing a cell vector/array are ignored')
    outStruct = rmfield(outStruct,fieldNames(cellInd));
end

% Look for structures and check if any dimension is defined
structInd = find(strcmp(outCell(:,1),'struct'));
if (~(isempty(structInd)))
    
    locCDF_Dimensions = cell(0,3);
    for (iField = structInd(:)')
        
        fn = fieldNames{iField};
        if (~isfield(outStruct.(fn),'data'))
            warning('struct4cdf:noData','Field "%s" is ignored as it contains no data.',fn);
            outStruct = rmfield(outStruct,fn);
        else
            
            if (isfield(outStruct.(fn),'dimName')),
                
                if (isfield(outStruct.(fn),'size')), locDim = outStruct.(fn).size;
                else
                    
                    locDim = size(outStruct.(fn).data);
                    if (isequal(locDim,[1 1])), locDim = 1;
                    else locDim = setdiff(locDim,1);
                    end
                    outStruct.(fn).size = locDim;
                    
                end
                cdfDimNew         = cat(2,outStruct.(fn).dimName(:),num2cell(locDim(:)),repmat({class(outStruct.(fn).data)},length(locDim),1));
                locCDF_Dimensions = cat(1,locCDF_Dimensions,cdfDimNew);
                
            end
            if (~isfield(outStruct.(fn),'type')), outStruct.(fn).type = class(outStruct.(fn).data); end
        
        end
        
    end
    if (~isempty(locCDF_Dimensions))
        [~,dimInd]        = unique(locCDF_Dimensions(:,1)); % 'legacy' or '2012a' don't matter here
        locCDF_Dimensions = locCDF_Dimensions(dimInd,:);
    end
    if (~isempty(CDF_Dimensions))
        [dimRedundant,betterClass]                  = ismember(locCDF_Dimensions(:,1),CDF_Dimensions(:,1));
        CDF_Dimensions(betterClass(dimRedundant),:) = locCDF_Dimensions(dimRedundant,:);
        locCDF_Dimensions(dimRedundant,:) = [];
    end
    CDF_Dimensions = cat(1,locCDF_Dimensions,CDF_Dimensions);
    
end

% Then strings
charInd  = find(strcmp(outCell(:,1),'char'));
if (~isempty(charInd))
    
    for (iField = charInd(:)')
        
        fn   = fieldNames{iField};
        fc   = outStruct.(fn);
        len  = max(2^ceil(log(size(fc,2))/log(2)),1);
        fr   = max(size(fc,1),1);
        lab1 = sprintf('_%i_byte_string',len);
        if (any(fn == '_'))
            lab2 = regexp(fn,'^(\w*?)_','tokens');
            lab2 = lab2{1}{1};
        else lab2 = fn;
        end
        lab2           = sprintf('%s_number',lab2);
        fc             = cat(2,fc,zeros(fr,len - size(fc,2)))';
        if (isempty(CDF_Dimensions) || ~any(strcmp(CDF_Dimensions(:,1),lab1))), CDF_Dimensions = cat(1,CDF_Dimensions,{lab1,len,'char'}); end
        if (isempty(CDF_Dimensions) || ~any(strcmp(CDF_Dimensions(:,1),lab2))), CDF_Dimensions = cat(1,CDF_Dimensions,{lab2,fr,'char'});  end
        outStruct.(fn) = struct('type','NC_CHAR','size',[len;fr],'dimName',{{lab1;lab2}},'data',fc);
        
    end
    
end

% Then numbers
numInd = find(~ismember(outCell(:,1),{'char','struct','cell'}));
if (~isempty(numInd))
    
    for (iField = numInd(:)')
        
        fn  = fieldNames{iField};
        fs  = size(outStruct.(fn));
        if (fs(end) == 1 && length(fs) > 1), fs = fs(1:length(fs) - 1); end
        fCl       = class(outStruct.(fn));
        matchDims = CDF_Dimensions(ismember(CDF_Dimensions(:,3),{fCl,'indeterminate'}'),:);
        labs      = cell(length(fs),1);
        for (iDim = 1:length(fs))
            
            mF = find([matchDims{:,2}] == fs(iDim));
            if (~isempty(mF))
                labs{iDim} = matchDims{mF(1),1};
                if (strcmp(matchDims{mF(1),3},'indeterminate')), CDF_Dimensions{strcmp(CDF_Dimensions(:,1),matchDims{mF(1),1}),3} = fCl; end % cannot handle dimensions used for multiple classes
                if (length(mF) > 1), warning('struct4cdf:nonUnivocalDim','Some dimensions are not univocally defined, which may result it wrong dimension attribution.'); end
            else
                
                if (any(fn == '_'))
                    lab2 = regexp(fn,'^(\w*?)_','tokens');
                    lab2 = lab2{1}{1};
                else lab2 = fn;
                end
                lab2 = sprintf('%s_number',lab2);
                labs{iDim} = lab2;
                CDF_Dimensions = cat(1,CDF_Dimensions,{lab2,fs(iDim),fCl});
                
            end
        end
        outStruct.(fn) = struct('type',fCl,'size',fs,'dimName',{labs},'data',outStruct.(fn));
        
    end
    
end

% Test unique dimensions
uniqueCDF = CDF_Dimensions(:,1);
for (iDim = 1:length(uniqueCDF)), uniqueCDF{iDim} = sprintf('%s%i',CDF_Dimensions{iDim,1:2}); end
[~,uniqueCDF]   = unique(uniqueCDF);
CDF_Dimensions  = CDF_Dimensions(uniqueCDF,1:2);
if (length(unique(CDF_Dimensions(:,1))) ~= size(CDF_Dimensions,1))
    disp(CDF_Dimensions)
    error('struct4cdf:repeatedNames','Same names for different dimensions are not allowed');
end
strDims = find(~cellfun(@isempty,regexp(CDF_Dimensions(:,1),'_string$')));
[~,ord] = sort([CDF_Dimensions{strDims,2}]);
CDF_Dimensions(strDims,:) = CDF_Dimensions(strDims(ord),:);
outStruct.CDF_Dimensions  = CDF_Dimensions;
if (~isempty(globAtt)), % Check global attributes
    
    if (~isstruct(globAtt)), error('struct4cdf:globNoStruct','The "GlobalAttribute" field must be a structure'); end
    for (iField = fieldnames(globAtt)')
        nField = iField{1};
        if (iscellstr(globAtt.(nField)))
            globAtt.(nField) = char(globAtt.(nField));
        else
            if (~(isnumeric(globAtt.(nField)) || ischar(globAtt.(nField))))
                error('struct4cdf:invalidGlobal','Global attributes can only be numeric or character arrays')
            end
        end
    end
    outStruct.Global_Attributes = globAtt;
    
end

end
