function [Outcome] = struct2cdfGroup(fileName, S, groupName, varargin)
% Write structure in a cdf file
%
% [Outcome] = struct2cdfGroup(fileName, S, groupName, Fields, compLevel)
%
% INPUT
% fileName : cdf file name
% S        : structure to copy in the group
% groupName: name of the group in the netcdf file
% Fields   : fields that are saved
% compLevel: a number between 0 (no compression) and 9 (maximum compression)
% overWrite: if true overwriting is allowed (default: false)
%
% OUTPUT
% Outcome  : TRUE if the writing was succesful.
%
% Author: Giorgio Tomasi
%         Giorgio.Tomasi@gmail.com
%
% Created      : 27 August 2013; 16:30
%
% Check n. of input
narginchk(2,5)

if (nargin < 3), groupName = ''; end

% Check filename
if (~isstruct(S)), error('struct2cdfGroup:invalidStruct','S must be a structure'); end

% Write the cdf
if (isempty(groupName))
    Outcome = struct2cdf(fileName,S,'netcdf4',varargin{:});
    return
elseif (~ischar(groupName)), error('struct2cdfGroup:invalidGroup','Invalid group name');
elseif (ischar(fileName))
    
    [pathStr,fileName,Ext] = fileparts(fileName);
    if (~(isempty(Ext) || strcmpi(Ext,'.cdf')))
        warning('struct2cdfGroup:badExtension','''cdf'' is the only extension allowed')
    end
    fileName = fullfile(pathStr,[fileName,'.cdf']);
    if (~exist(fileName,'file'))
        warning('struct2cdfGroup:fileDoesNotExist','The file does not exist and is created from scratch.')
        ncid = netcdf.create(fileName,bitor(netcdf.getConstant('NOCLOBBER'),netcdf.getConstant('NETCDF4')));
        netcdf.endDef(ncid)
    else
        ncid = netcdf.open(fileName,'write');
    end
    
elseif (isnumeric(fileName)), ncid = fileName;
else
    error('struct2cdfGroup:invalidFileName','fileName must be a file name or a file/group identifier')
end
if (~strcmp(netcdf.inqFormat(ncid),'FORMAT_NETCDF4')), error('struct2cdfGroup:invalidCDFFormat','Invalid "cdf" format'); end
try
    
    grpIDs = netcdf.inqGrps(ncid);
    if (~isempty(grpIDs))
        for (iGrp = 1:length(grpIDs))
            if (strcmp(groupName,netcdf.inqGrpName(grpIDs(iGrp))))
                error('struct2cdfGroup:groupExists','The group already exists (overwriting is currently not available)')
            end
        end
    end
    grpid   = netcdf.defGrp(ncid,groupName);
    Outcome = struct2cdf(grpid,S,'netcdf4',varargin{:});
    
catch ME
    fprintf('Error: %s\n',ME.message)
    if nargout > 0, Outcome = false; end
end
if (~isnumeric(fileName)), netcdf.close(ncid); end
end
