<<<<<<< HEAD
function [mzroi,MSroi,mzBinned,ScanNumber,roicell]=ROIpeaks(peaks,thresh,mzerror,minroi,CheckEvery,s,e,nrows,time)
nroi = 0;
mzroi = NaN(0,5);
=======
function [mzroi,MSroi,roicell]=ROIpeaks(peaks,thresh,mzerror,minroi,nrows,time)
% function [mzroi,MSroi,roicell]=ROIpeaksnew(peaks,thresh,mzerror,minroi,nrows,time)
%
% This function allows building a MS data matrix from structure variable peaks
% selecting only the regions of interest (roi). These regions are defined acording to
% the following input parameters: thresh, mzeror and minroi
%
% INPUT
% % peaks is the cell variable containing MS measurements with as many cells
% as MS spectra/retention times (nrows). In every cell, mz and MS intensities
% are given for every spectrum (every cell/MS spectrum has different lengths)
% % thresh is a parameter to filter significative MS intensities,
% i.e thresh = 0.1% max measured intensity (max(max(MSroi))
% % mzerror is a parameter to define the width of mz experimental values in peaks
% to be considered coming from the same theoreical mz value
% % minroi minimum number of elution times to be considered in a roi (e.g. 3)
% % nrows number of cells/rows/spectra of the variable peaks to be processed
% % time elution (retention) times corresponding to cells/rows/spectra
%
% OUTPUT
% % mzroi, finally selected mz values (in the same roicell)
% % MSroi, new arrenged MS spectra data matrix of dimensions
%   (nr.of MS spectra), nr.of mzroi values )
% % roicell contents of selected ROI arranged in a cell data set,
% in the output they are finally ordered according to mzroi values
% roicell{:,1}= individual measured mz values in every ROI (one
% roicell(:,2}= elution times where couples mz,MSI are measured
% roicell{:,3}= measure MSI intensities at time, mz
% roicell{:,4}= consiedered row of peaks file (elution time);
% roicell{:,5}= mzroi, final mz balue of the considered ROI (mean of all mz values
% included in ROI
%
% e.g. mzroi,MSroi,roicell]=ROIpeaksnew(peaks,1000,0.01,10,1899,time);
% where thresh=1000, mzerror=0.01 and minroi=10
% background in MSroi is MSroi=randn(nrows,nmzroi).*0.3*thresh;
%%
% [mzroi,MSroi,roicell]=ROIpeaks(peaks,thresh,mzerror,minroi,nrows,time)

tic;
tstart=tic;
disp('number of spectra (elution times) to process is: ');disp(nrows)
mzroi=[];
roicell{1,1}=[];
roicell{1,2}=[];
roicell{1,3}=[];
roicell{1,4}=[];
nmzroi=1;
% close all

% looking for mzroi values
>>>>>>> 7bedca5add62ca748b16e54455b4c04afb0056c6

nroi = 0;
for irow=1:nrows
<<<<<<< HEAD
    scanIntensities=[];
    A     = double(peaks{irow});
    ipeak = A(:,2) >= thresh;
    
=======
    
    disp('MS spectrum (elution time) being processed is: ');disp(irow)
    A     = double(peaks{irow});
    ipeak = A(:,2) >= thresh;
    mzroi = NaN(0,5);
>>>>>>> 7bedca5add62ca748b16e54455b4c04afb0056c6
    if any(ipeak)
        
        mz = A(ipeak,1);
        MS = A(ipeak,2);
        if (irow == 1), [newMZ,newMS] = deal(mz,MS);
<<<<<<< HEAD
            
=======
>>>>>>> 7bedca5add62ca748b16e54455b4c04afb0056c6
        else
            
            wL                               = mzroi(:,1) - mzerror/2;
            wU                               = mzroi(:,1) + mzerror/2;
<<<<<<< HEAD
            [scanPointRoiIndex,nPointsInROI] = basicBin(1,length(mz),zeros(size(mz)),length(newMS),mz,MS,wL,wU); % assumes mzroi is sorted in ascending order
            pointsInRoIIndex                 = scanPointRoiIndex > 0;
            scanIntensities                  = accumarray(scanPointRoiIndex(pointsInRoIIndex),MS(pointsInRoIIndex),[length(mzroi),1]);
=======
            [scanPointRoiIndex,nPointsInROI] = basicBin(1,length(mz),zeros(size(mz)),nPointsInROIOld,mz,MS,wL,wU); % assumes mzroi is sorted in ascending order
            pointsInRoIIndex                 = scanPointRoiIndex > 0;
            scanIntensities                  = accumarray(scanPointRoiIndex(pointsInRoIIndex),1,MS(pointsInRoIIndex),[length(mzroi),1]);
>>>>>>> 7bedca5add62ca748b16e54455b4c04afb0056c6
            newInd                           = scanPointRoiIndex < 1;
            newMZ                            = mz(newInd);
            newMS                            = MS(newInd);
            
        end
        if (~isempty(newMZ))
            [newRoI,scanPointNewRoiIndex,scanIntensities] = processNewMZ(newMZ,newMS,mzerror,nroi);
            
        end
        
        
        %         nmz=size(mz);
        %
        %         for i=1:nmz
        %
        %             %ieq=find(abs(mzroi-mz(i))<=mzerror/2);
        %             ieq=find(abs(mzroi-mz(i))<=mzerror);
        %
        %
        %             if isfinite(ieq)
        %                 ieq=ieq(1);
        %                 roicell{ieq,1}=[roicell{ieq,1},mz(i)];
        %                 roicell{ieq,2}=[roicell{ieq,2},time(irow)];
        %                 roicell{ieq,3}=[roicell{ieq,3},MS(i)];
        %                 roicell{ieq,4}=[roicell{ieq,4},irow];
        %                 roicell{ieq,5}=sum(roicell{ieq,1}/size(roicell{ieq,1},2));
        %                 mzroi(ieq)=roicell{ieq,5};
        %                 % disp('mzroi is now'),disp(mzroi(ieq))
        %
        %             else
        %
        %                 %                 disp('roi expansion'),
        %                 nmzroi =nmzroi+1;
        %                 roicell{nmzroi,1}=mz(i);
        %                 roicell{nmzroi,2}=time(irow);
        %                 roicell{nmzroi,3}=MS(i);
        %                 roicell{nmzroi,4}=irow;
        %                 roicell{nmzroi,5}=mz(i);
        %                 mzroi(nmzroi)=mz(i);
        %                 % disp('mzroi expanded is'),disp(mzroi(nmzroi))
        %
        %             end
        %
        %         end
        
        if (~isempty(newMZ))
            [newRoI,~,scanIntensities] = processNewMZ(newMZ,newMS,mzerror,nroi);
            
            if irow==1
                MSroi(:,1) = scanIntensities;
                mzBinned(:,1)   = newRoI;
            end
            roicell{1,irow}(1,:)=newRoI;
            roicell{2,irow}(1,:)=scanIntensities;
        end 
    end
    
end

[mzroi,MSroi,mzBinned,ScanNumber] = CombTwoScans(roicell(1,:),roicell(2,:),mzerror,thresh,minroi,CheckEvery,s,e,nrows);

mzroi=mzroi;
MSroi=MSroi;
mzBinned=mzBinned;

<<<<<<< HEAD
=======
% pause
roicellsort=cell(nmzroi,5);
for i=1:nmzroi,for j=1:5,roicellsort{i,j}=roicell{isort(i),j};end,end
roicell=roicellsort;
% disp('');disp('');

% Now, filter those having a minimum number of elution times (minroi)
% and a maximum value higher than thresh
numberroi=zeros(1,nmzroi);
for i=1:nmzroi
    if isempty(roicell{i,1}),roicell{i,1}=0;end
    numberroi(1,i)=length(roicell{i,1});
end
maxroi=zeros(1,nmzroi);
for i=1:nmzroi
    if isempty(roicell{i,3}),roicell{i,3}=0;end
    maxroi(i)=max(roicell{i,3});
>>>>>>> 7bedca5add62ca748b16e54455b4c04afb0056c6
end


function [a,b] = basicBin(s,e,a,b,x,~,wL,wU)
% a: vector with current mzRoi index for each value in the scan
% b: number of points currently in the regions
n = length(wL);
c = 1;
while (c <= n && s <= e)
    if (x(s) > wU(c)), c = c + 1; continue; end
    if (x(s) >= wL(c)), a(s) = c; b(c) = b(c) + 1; end
    s = s + 1;
end

end
function [newRoI,scanPointNewRoiIndex,scanIntensities] = processNewMZ(newMZ,newMS,mzerror,nroi)
newRoI               = 0;
scanIntensities      = 0;
% scanPointNewRoiIndex = 0;
% newmzBinned=nan;
mzDistance = (newMZ(2:end) - newMZ(1:end - 1)) <= mzerror/2;
mzDistance = -diff([false;mzDistance;false]); % FIXME: the next line is simpler if there is no minus
mzDistance = cumsum(cat(1,1,cumsum(mzDistance) + 1));
mzDistance = mzDistance(1:end - 1);
count      = accumarray(mzDistance,1);
pos        = cumsum(cat(1,0,accumarray(mzDistance,1)));
c          = 0;
scanPointNewRoiIndex = zeros(0,1);
for (i = 1:length(count) - 1)
    if (count(i) == 1)
        c                  = c + 1;
        newRoI(c)          = newMZ(pos(i + 1));  %#ok<*AGROW>
        scanIntensities(c) = newMS(pos(i + 1));
    else
        
        nPts  = count(i) ;
        wL    = newMZ(pos(i) + 1) - mzerror/2:mzerror:newMZ(pos(i + 1)) + mzerror/3;
        wU    = wL + mzerror;
        mzLoc = newMZ(pos(i) + 1:pos(i + 1));
        iLoc  = newMS(pos(i) + 1:pos(i + 1));
        len   = length(wL);
        [scanPointRoiIndex,nPointsInROI] = basicBin(1,nPts,zeros(nPts,1),zeros(len,1),mzLoc,[],wL,wU);
        if sum(nPointsInROI==0)>0
            len= length(wL)-1;
        else
            len = length(wL);
        end
        newRoI(c + 1:c + len) = accumarray(scanPointRoiIndex,mzLoc) ./ nPointsInROI(1:len);
        scanPointNewRoiIndex  = cat(1,scanPointNewRoiIndex,scanPointRoiIndex + nroi + c);
        scanIntensities(c + 1:c + len) = accumarray(scanPointRoiIndex,iLoc) ./  nPointsInROI(1:len);
        c                              = c + len;
    end
    
end


<<<<<<< HEAD
end

=======
for i=1:nmzroi
    nval=length(roicell{i,4});
    for j=1:nval
        irow=roicell{i,4}(j);
        MSI=roicell{i,3}(j);
        MSroi(irow,i)=MSroi(irow,i)+MSI;
    end
    % Fill MSroi empty times in mzroi, specially those with zeros inside
    % a chromatograhic peak
    y=MSroi(:,i);
    iy=find(y>0);
    intertime=[time(iy(1):iy(end))];
    ynew = interp1(time(iy), y(iy), intertime);
    MSroi([iy(1):1:iy(end)],i)=ynew;
    MSroi(:,i)=MSroi(:,i)+randn(nrows,1).*0.3*thresh;
end

%
% disp('displaying MS spectra only ROI mz values')
% figure
% bar(mzroi,sum(MSroi),mzerror)
%
% disp('displaying chromatograms only at ROI mz values')
% figure
% plot(time(1:nrows),MSroi)
>>>>>>> 7bedca5add62ca748b16e54455b4c04afb0056c6

function[mzroi,MSroi,mzBinned,ScanNumber] = CombTwoScans(mz_cell,Int_cell,mzerror,thresh,minroi,CheckEvery,s,e,nrows)

<<<<<<< HEAD
for irow=1:nrows
       fprintf(1,'Scan: %i\n',irow);
    if irow==1
        mzOld  = mz_cell{irow};
        mzroi         = mz_cell{irow}';
        MSroi(:,1)    = Int_cell{irow}';
        mzBinned(:,1) = mz_cell{irow}';
        ScanNumber    = irow;
     
    else
        NewIntLoc=zeros(1,1);
        PosIntNew=zeros(1,1);
        NewLoc=zeros(1,1);
        PosNew=zeros(1,1);
        PosMZ=zeros(1,1);
        NewIntLoc=zeros(1,1);
        intLoc=zeros(1,1);
        mzLoc=zeros(1,1);
        
        mzOld=mzroi;
        mzNew  = mz_cell{irow};
        IntNew = Int_cell{irow};
        
        %Calculates lower and upper boundary of the bin
        wL=mzOld-mzerror/2;
        wU=mzOld+mzerror/2;
        
        %resets counters to 1
        k=1;i=1;s=1;f=1; h=1;d=1;
        while i<=length(mzOld)
            while k <= length(mzNew)&& s<=length(wU)&& i <=length(wU)%&& abs(N(k)-O(i))<=mzerror/2
                if mzNew(k)>wU(s)
                    f           = 1;
                    mzLoc(i,f)  = mzOld(i);
                    PosMZ(i)    = h;
                    intLoc(i,f) = 0;
                     h=h+1;
                    i          = i+1;
                    s          = s+1;
                    continue
                   
                end
                if mzNew(k)>=wL(s)
                    f           = f+1;
                    mzLoc(i,f)  = mzNew(k);
                    intLoc(i,f) = IntNew(k);   
                    PosMZ(h)    = h;
                else
                    NewLoc(d)    = mzNew(k);
                    NewIntLoc(d) = IntNew(k);
                    PosNew(i)       = h;
                    h =h+1;
                    d=d+1;
                end
                k=k+1;
            end
         
            if i<=length(mzOld)
                mzLoc(i,1)=mzOld(i);
                intLoc(i,1)=0;
                PosMZ(i) = h;
            h=h+1;
            end
            
            %Finds the ROIs in the new mz vector which were not present in the current ROI
            while k<=length(mzNew)
                NewLoc(d)    = mzNew(k);
                NewIntLoc(d) = IntNew(k);
                PosNew(d)    = h;
                h=h+1;
                d            = d+1;
                k            = k+1;
            end
            i=i+1;
        end
        
        % calculates the mean of the mz value across the two scans and int within
        % the scan
        
        % calculates the mean of the mz value across the two scans and int within
        % the scan
        
        mzLoc(mzLoc==0)=nan;
        intLoc(intLoc==0)=nan;
        intLoc=[mean(intLoc,2,'omitnan');NewIntLoc(NewIntLoc>0)'];
        ScanNumber=cat(2,ScanNumber,repelem(irow,size(mzLoc,2)));
        
   if isempty(NewLoc(NewLoc>0))
        mzBinnedOld=mzLoc;
   else 
        mzBinnedOld=cat(1,mzLoc,[NewLoc(NewLoc>0)',nan(length(NewLoc(NewLoc>0)'),size(mzLoc,2)-1)]);
   end 
        mzroi=[mean(mzLoc,2,'omitnan');NewLoc(NewLoc>0)'];
        
        nroi=0;
        %
        %processNewMZ(newMZ,newMS,thresh,mzerror,nroi)
        
        
        dif=length(mzroi)-length(MSroi(:,irow-1));
        
        if dif>=0
            mzBinned=cat(1,mzBinned,nan(dif,size(mzBinned,2)));
            mzBinned=cat(2,mzBinned,mzBinnedOld);
            MSroi=cat(1,MSroi,zeros(dif,size(MSroi,2)));
            MSroi(:,irow)=intLoc;
        end
    end
    mzBinned(mzBinned==0)=nan;
    
    % filters out mz's with less consecutive point than minroi
    if sum(irow==[minroi*2:CheckEvery:e-s])>0
   sortMatrix=sortrows(cat(2,mzroi,MSroi,mzBinned));
    mzroi=sortMatrix(:,1);
    MSroi=sortMatrix(:,2:size(mzroi,2)+size(MSroi,2));
    mzBinned=sortMatrix(:,size(mzroi,2)+size(MSroi,2)+1:end);
    [MSroi,mzroi,mzBinned]=MinROICheck(mzroi,MSroi,mzBinned,minroi);

    end
    %Output
    sortMatrix=sortrows(cat(2,mzroi,MSroi,mzBinned));
    mzroi=sortMatrix(:,1);
    MSroi=sortMatrix(:,2:size(mzroi,2)+size(MSroi,2));
    mzBinned=sortMatrix(:,size(mzroi,2)+size(MSroi,2)+1:end);
   mzBinned(mzBinned==0)=nan;
end

end
=======
disp('elapsed time: ');disp(toc(tstart))

end


function [a,b] = basicBin(s,e,a,b,x,~,wL,wU)
% a: vector with current mzRoi index for each value in the scan
% b: number of points currently in the regions
n = length(wL);
c = 1;
while (c <= n && s <= e)
    if (x(s) > wU(c)), c = c + 1; continue; end
    if (x(s) >= wL(c)), a(s) = c; b(c) = b(c) + 1; end
    s = s + 1;
end

end

function [newRoI,scanPointNewRoiIndex,scanIntensities] = processNewMZ(newMZ,newMS,mzerror,nroi)
mzDistance = (newMZ(2:end) - newMZ(1:end - 1)) <= mzerror/2;
mzDistance = -diff([false;mzDistance;false]); % FIXME: the next line is simpler if there is no minus
mzDistance = cumsum(cat(1,1,cumsum(mzDistance) + 1));
mzDistance = mzDistance(1:end - 1);
count      = accumarray(mzDistance,1);
pos        = cumsum(cat(1,0,accumarray(mzDistance,1)));
c          = 0;
scanPointNewRoiIndex = zeros(0,1);
for (i = 1:length(count) - 1)
    if (count(i) == 1)
        c                  = c + 1;
        newRoI(c)          = newMZ(pos(i + 1));  %#ok<*AGROW>
        scanIntensities(c) = newMS(pos(i + 1));
    else
        
        nPts  = count(i) ;
        wL    = newMZ(pos(i) + 1) - mzerror/2:mzerror:newMZ(pos(i + 1)) + mzerror/3;
        wU    = wL + mzerror;
        mzLoc = newMZ(pos(i) + 1:pos(i + 1));
        iLoc  = newMS(pos(i) + 1:pos(i + 1));
        len   = length(wL);
        [scanPointRoiIndex,nPointsInROI] = basicBin(1,nPts,zeros(nPts,1),zeros(len,1),mzLoc,[],wL,wU);
        newRoI(c + 1:c + len) = accumarray(scanPointRoiIndex,mzLoc) ./ nPointsInROI(1:len);
        scanPointNewRoiIndex  = cat(1,scanPointNewRoiIndex,scanPointRoiIndex + nroi + c);
        scanIntensities(c + 1:c + len) = accumarray(scanPointRoiIndex,iLoc);
        c                              = c + len;
        
    end
    
end


end
>>>>>>> 7bedca5add62ca748b16e54455b4c04afb0056c6
