
%%
function[mzroi,MSroi,mzBinned] = CombTwoScans(mz_cell,Int_cell,mzerror,thresh,minroi,nrows)

for irow=1:nrows
    disp('irow=')
    irow
    if irow==1
        mzOld         = mz_cell{irow};
        mzroi         = mz_cell{irow}';
        MSroi(:,1)    = Int_cell{irow}';
        mzBinned(:,1) = mz_cell{irow}';
    else
        NewIntLoc=zeros(1,1);
        NewLoc=zeros(1,1);
        NewIntLoc=zeros(1,1);
        intLoc=zeros(1,1);
        mzLoc=zeros(1,1);
        mzIntPos=0;
        mzPos=0;
        oldPos=0;
        oldIntPos=0;
        
        mzOld=mzroi;
        mzNew  = mz_cell{irow};
        IntOld = MSroi;
        IntNew = Int_cell{irow};
        
        %Calculates lower and upper boundary of the bin
        wL=mzOld-mzerror/2;
        wU=mzOld+mzerror/2;
        
        %resets counters to 1
        k=1;i=1;s=1;f=1; h=1;d=1;
        while i<=length(mzOld)
            while k <= length(mzNew)&& s<=length(wU)&& i <=length(wU)%&& abs(N(k)-O(i))<=mzerror/2
                if mzNew(k)>wU(s)
                    f           = 1;
                    mzLoc(i,f)  = mzOld(i);
%                     intLoc(i,f) = Int;
                    oldPos(i)   = h;
                    h=h+1;
                    
                    i          = i+1;
                    s          = s+1;
                    continue
                    h=h+1;
                end
                if mzNew(k)>=wL(s)
                    f           = f+1;
                    mzLoc(i,f)  = mzNew(k);
%                     mzPos(h)    = h;
                    oldIntPos(i)   = h;
                    intLoc(i,f) = IntNew(k);
%                     h=h+1;
                    
                else
                    NewLoc(d)    = mzNew(k);
                    NewIntLoc(d) = IntNew(k);
                    d=d+1;
                     mzPos(h)    = h;
                      mzIntPos(h)    = h;
               
                    h=h+1;
                    
                end
                k=k+1;
            end
            
            if i<=length(mzOld)
                mzLoc(i,1)=mzOld(i);
                 mzPos(h)    = h;
             
                 oldPos(i)   = h;
                 h=h+1
                intLoc(i,1)=0;end
            %Finds the ROIs in the new mz vector which were not present in the current ROI
            while k<=length(mzNew)
                NewLoc(d)    = mzNew(k);
                NewIntLoc(d) = IntNew(k);
                         mzPos(h)    = h;
                          mzIntPos(h)    = h;
                         
                         h=h+1;
                d            = d+1;
                k            = k+1;
            end
            i=i+1;
        end
        
        % calculates the mean of the mz value across the two scans and int within
        % the scan
        
        % calculates the mean of the mz value across the two scans and int within
        % the scan

        mzLoc(mzLoc==0)=nan;
        intLoc(intLoc==0)=nan;
%         intLoc=[mean(intLoc,2,'omitnan');NewIntLoc(NewIntLoc>0)'];
%         
        mzBinned=mzLoc(:,2:end);
        
%         mzroi=[mean(mzLoc,2,'omitnan');NewLoc(NewLoc>0)'];
        
 
%MSroi
X=zeros(length(mzPos),irow);
X( [ oldPos(oldPos>0)],1:irow-1)=MSroi;
X( [ mzIntPos(mzIntPos>0)] ,irow)=NewIntLoc(NewIntLoc>0)';
X( [ oldIntPos(oldIntPos>0)],irow)=mean(intLoc(intLoc>0),2,'omitnan');
MSroi=X;
%mzroi
mzroi([ mzPos(mzPos>0)] )=NewLoc(NewLoc>0)';
mzroi( [ oldPos(oldPos>0)])=mean(mzLoc,2,'omitnan');

        
    % filters out mz's with less consecutive point than minroi
    if sum(irow==[100:100:2020,2020])>0
  
        [MSroi,mzroi,mzBinned]=MinROICheck(mzroi,MSroi,mzBinned,minroi);
        
        [mzroi,~,MSroi] = processNewMZ2(mzroi,MSroi,1,mzerror,nroi);
        mzroi=mzroi';
    end

    
    
end

end