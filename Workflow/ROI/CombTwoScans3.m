
function[mzroi,MSroi,mzBinned] = CombTwoScans(mz_cell,Int_cell,mzerror,nrows)
%This function bins two vectors using the the mz values of the first vector
%(mzold) and calculates the average mz, intensity and stores the raw values
%of the mz's

%Input:
%mzOld: is a vector with mz values used to calculate the mass bins for
%the new vector to be binned towards
%mzNew: The new vector to be binned to the mzOld. If this vector
%contains mz values not contained by mzOld a new ROI will be initiated.
%mzerror: the mass accuracy measured in Da
%IntNew: Intensity measurements of the new vector


%Output:
%mzroi is a vector with the new ROI calculated from mzOld and mzNew
%MSroi is a vector with the intensities of the new ROI for the new
%vector
%mzBinned is matrix with the raw mz values binned. This can be used to
%evaluate whether there are drift occuring in the mz dimension due to
%this being a greedy algorithm
%Initialize matrices
% function[mzroi,MSroi,mzBinned] = CombTwoScans(mz_cell,Int_cell,mzerror,nrows)

mzOld  = mz_cell{1};
% mzroi_temp=cell(size(stepIrow,1),1);
% MSroi_temp=cell(size(stepIrow,1),1);
% mzBinned_temp=cell(size(stepIrow,1),1);

% for (Int=1:size(stepIrow,1))
% for (irow=stepIrow(Int,1):stepIrow(Int,2))
    for (irow = 1:nrows)
        
disp(irow)
        if irow==1
            mzroi         = mz_cell{irow}';
            MSroi(:,1)    = Int_cell{irow}';
            mzBinned(:,1) = mz_cell{irow}';
        else
            mzOld=mzroi;
            mzNew  = mz_cell{irow};
            IntNew = Int_cell{irow};

            %Calculates lower and upper boundary of the bin
            wL=mzOld-mzerror/2;
            wU=mzOld+mzerror/2;
            
            %resets counters to 1
            k=1;i=1;s=1;f=1; h=1;
            while i<length(mzOld)
                while k <= length(mzNew) && s<=length(wU)&& i <=length(wU)
                    if mzNew(k)>wU(s)
                        f           = 1;
                        mzLoc(h,f)  = mzOld(i);
                        intLoc(h,f) = 0;
                        i          = i+1;
                        h=h+1;
                        s          = s+1;
                        while s>length(wU)&&k <= length(mzNew)
                            mzLoc(h,2)=mzNew(k);
                            intLoc(h,2)=IntNew(k);
                            h=h+1;
                            k=k+1;
                        end
                        continue
                        
                    end
                    if mzNew(k)>=wL(s)
                        f           = f+1;
                        mzLoc(h,f)  = mzNew(k);
                        intLoc(h,f) = IntNew(k);
                    else
                        f=2;
                        mzLoc(h,f)    = mzNew(k);
                        intLoc(h,f) = IntNew(k);
                        h=h+1;
                    end
                    k=k+1;
                end
                
                while i<=length(mzOld)
                    
                    mzLoc(h,1)=mzOld(i);
                    intLoc(h,1) = 0;
                    i=i+1;
                    h=h+1;
                end
                %Finds the ROIs in the new mz vector which were not present in the current ROI
                
                i=i+1;
               
            end
            
            % calculates the mean of the mz value across the two scans and int within
            % the scan
            mzLoc(mzLoc==0)=nan;
            intLoc(intLoc==0)=nan;
            mzBinned=mzLoc(:,2:end);
            mzroi=mean(mzLoc,2,'omitnan');
            dif=length([sum(intLoc,2)./sum(isnan(intLoc),2)])-length(MSroi(:,irow-1));
            if dif>0
                mzBinned=cat(1,mzBinned,zeros(dif,size(mzBinned,2)));
                MSroi=cat(1,MSroi,zeros(dif,size(MSroi,2)));
                MSroi(:,irow)=[sum(intLoc,2)./sum(isnan(intLoc),2)]';
            else
                MSroi(:,irow)=cat(2,[sum(intLoc,2)./sum(isnan(intLoc),2)]',zeros(1,-dif));
            end
            
        end
        
    end
%     %Output
    MSroi(MSroi==0)=nan;
%     MSroi_temp{Int}=MSroi;
    mzBinned(mzBinned==0)=nan;
%     mzBinned_temp{Int}=mzBinned;

end
% end