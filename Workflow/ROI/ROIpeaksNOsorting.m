function [mzroi,MSroi,mzBinned,ScanNumber,roicell]=ROIpeaks(peaks,thresh,mzerror,minroi,CheckEvery,s,e,nrows,time)

tic;
tstart=tic;
disp('number of spectra (elution times) to process is: ');disp(nrows)
mzroi=[];
roicell{1,1}=[];
roicell{1,2}=[];
roicell{1,3}=[];
roicell{1,4}=[];
nmzroi=1;
% close all

% looking for mzroi values

nroi = 0;
mzroi = NaN(0,5);

for irow=1:nrows
    scanIntensities=[];
    A     = double(peaks{irow});
    ipeak = A(:,2) >= thresh;
    
    if any(ipeak)
        
        mz = A(ipeak,1);
        MS = A(ipeak,2);
        if (irow == 1), [newMZ,newMS] = deal(mz,MS);
            
        else
            
            wL                               = mzroi(:,1) - mzerror/2;
            wU                               = mzroi(:,1) + mzerror/2;
            [scanPointRoiIndex,nPointsInROI] = basicBin(1,length(mz),zeros(size(mz)),length(newMS),mz,MS,wL,wU); % assumes mzroi is sorted in ascending order
            pointsInRoIIndex                 = scanPointRoiIndex > 0;
            scanIntensities                  = accumarray(scanPointRoiIndex(pointsInRoIIndex),MS(pointsInRoIIndex),[length(mzroi),1]);
            newInd                           = scanPointRoiIndex < 1;
            newMZ                            = mz(newInd);
            newMS                            = MS(newInd);
            
        end
        
        if (~isempty(newMZ))
            [newRoI,~,scanIntensities] = processNewMZ(newMZ,newMS,mzerror,nroi);
            
            if irow==1
                MSroi(:,1) = scanIntensities;
                mzBinned(:,1)   = newRoI;
            end
            roicell{1,irow}(1,:)=newRoI;
            roicell{2,irow}(1,:)=scanIntensities;
        end
        
        
        
        %         nmz=size(mz);
        %
        %         for i=1:nmz
        %
        %             %ieq=find(abs(mzroi-mz(i))<=mzerror/2);
        %             ieq=find(abs(mzroi-mz(i))<=mzerror);
        %
        %
        %             if isfinite(ieq)
        %                 ieq=ieq(1);
        %                 roicell{ieq,1}=[roicell{ieq,1},mz(i)];
        %                 roicell{ieq,2}=[roicell{ieq,2},time(irow)];
        %                 roicell{ieq,3}=[roicell{ieq,3},MS(i)];
        %                 roicell{ieq,4}=[roicell{ieq,4},irow];
        %                 roicell{ieq,5}=sum(roicell{ieq,1}/size(roicell{ieq,1},2));
        %                 mzroi(ieq)=roicell{ieq,5};
        %                 % disp('mzroi is now'),disp(mzroi(ieq))
        %
        %             else
        %
        %                 %                 disp('roi expansion'),
        %                 nmzroi =nmzroi+1;
        %                 roicell{nmzroi,1}=mz(i);
        %                 roicell{nmzroi,2}=time(irow);
        %                 roicell{nmzroi,3}=MS(i);
        %                 roicell{nmzroi,4}=irow;
        %                 roicell{nmzroi,5}=mz(i);
        %                 mzroi(nmzroi)=mz(i);
        %                 % disp('mzroi expanded is'),disp(mzroi(nmzroi))
        %
        %             end
        %
        %         end
        
    end
    
    
    
    
    % disp('initial mz ROI values'),disp(mzroi)
    % disp('initial nr of ROI'),disp(nmzroi)
    % [mzroi,isort]=sort(mzroi);
    
    % disp('mz ROI ordered values'),disp(mzroi)
    %
    % % pause
    % roicellsort=cell(nmzroi,5);
    % for i=1:nmzroi,for j=1:5,roicellsort{i,j}=roicell{isort(i),j};end,end
    % roicell=roicellsort;
    % % disp('');disp('');
    %
    % % Now, filter those having a minimum number of elution times (minroi)
    % % and a maximum value higher than thresh
    % numberroi=zeros(1,nmzroi);
    % for i=1:nmzroi
    %     if isempty(roicell{i,1}),roicell{i,1}=0;end
    %     numberroi(1,i)=length(roicell{i,1});
    % end
    % maxroi=zeros(1,nmzroi);
    % for i=1:nmzroi
    %     if isempty(roicell{i,3}),roicell{i,3}=0;end
    %     maxroi(i)=max(roicell{i,3});
    % end
    %
    % iroi=find(numberroi>minroi & maxroi>thresh);
    %
    % mzroi=mzroi(iroi);
    % nmzroi=length(mzroi);
    %
    % % disp('final mz ROI values'),disp(mzroi)
    % % disp('final nr of ROI'),disp(nmzroi)
    % % disp('roicell')
    % roicell=roicell(iroi,:);
    %
    % % pause
    %
    % % Evaluation of MS values from roicell{nmzroi,3}
    % % Now evaluating MS matrix only for thes mzroi values
    % % Defining first the backgound
    %
    % % disp('displaying MS spectra only ROI mz values')
    % % figure
    % % bar(mzroi,sum(MSroi),mzerror)
    % %
    % % disp('displaying chromatograms only at ROI mz values')
    % % figure
    % % plot(time(1:nrows),MSroi)
    %
    %     disp('final number of mz roi values considered has been: '),disp(nmzroi)
    %
    %     disp('elapsed time: ');disp(toc(tstart))
    
end

%         mzOld=roicell{1,irow-1}(1,:);
%         mzNew= roicell{1,irow}(1,:);
%
%         IntOld=roicell{2,irow-1}(1,:);
%         IntNew= roicell{2,irow}(1,:);
[mzroi,MSroi,mzBinned,ScanNumber] = CombTwoScans(roicell(1,:),roicell(2,:),mzerror,thresh,minroi,CheckEvery,s,e,nrows);

mzroi=mzroi;
MSroi=MSroi;
mzBinned=mzBinned;
%         MSroi(:,irow) = scanIntensities;
%         mzBinned      = cat(1,mzBinned,mzBinned_temp);



end


function [a,b] = basicBin(s,e,a,b,x,~,wL,wU)
% a: vector with current mzRoi index for each value in the scan
% b: number of points currently in the regions
n = length(wL);
c = 1;
while (c <= n && s <= e)
    if (x(s) > wU(c)), c = c + 1; continue; end
    if (x(s) >= wL(c)), a(s) = c; b(c) = b(c) + 1; end
    s = s + 1;
end

end
function [newRoI,scanPointNewRoiIndex,scanIntensities] = processNewMZ(newMZ,newMS,mzerror,nroi)
newRoI               = 0;
scanIntensities      = 0;
% scanPointNewRoiIndex = 0;
% newmzBinned=nan;
mzDistance = (newMZ(2:end) - newMZ(1:end - 1)) <= mzerror/2;
mzDistance = -diff([false;mzDistance;false]); % FIXME: the next line is simpler if there is no minus
mzDistance = cumsum(cat(1,1,cumsum(mzDistance) + 1));
mzDistance = mzDistance(1:end - 1);
count      = accumarray(mzDistance,1);
pos        = cumsum(cat(1,0,accumarray(mzDistance,1)));
c          = 0;
scanPointNewRoiIndex = zeros(0,1);
for (i = 1:length(count) - 1)
    if (count(i) == 1)
        c                  = c + 1;
        newRoI(c)          = newMZ(pos(i + 1));  %#ok<*AGROW>
        scanIntensities(c) = newMS(pos(i + 1));
    else
        
        nPts  = count(i) ;
        wL    = newMZ(pos(i) + 1) - mzerror/2:mzerror:newMZ(pos(i + 1)) + mzerror/3;
        wU    = wL + mzerror;
        mzLoc = newMZ(pos(i) + 1:pos(i + 1));
        iLoc  = newMS(pos(i) + 1:pos(i + 1));
        len   = length(wL);
        [scanPointRoiIndex,nPointsInROI] = basicBin(1,nPts,zeros(nPts,1),zeros(len,1),mzLoc,[],wL,wU);
        if sum(nPointsInROI==0)>0
            len= length(wL)-1;
        else
            len = length(wL);
        end
        newRoI(c + 1:c + len) = accumarray(scanPointRoiIndex,mzLoc) ./ nPointsInROI(1:len);
        scanPointNewRoiIndex  = cat(1,scanPointNewRoiIndex,scanPointRoiIndex + nroi + c);
        scanIntensities(c + 1:c + len) = accumarray(scanPointRoiIndex,iLoc) ./  nPointsInROI(1:len);
        c                              = c + len;
    end
    
end


end

function [newRoI,scanPointNewRoiIndex,scanIntensities] = processNewMZ2(newMZ,newMS,~,mzerror,nroi)
newRoI               = [];
scanIntensities      = [];
scanPointNewRoiIndex = [];
newmzBinned=[];
mzDistance = (newMZ(2:end) - newMZ(1:end - 1)) <= mzerror/2;
mzDistance = -diff([false;mzDistance;false]); % FIXME: the next line is simpler if there is no minus
mzDistance = cumsum(cat(1,1,cumsum(mzDistance) + 1));
mzDistance = mzDistance(1:end - 1);
count      = accumarray(mzDistance,1);
pos        = cumsum(cat(1,0,accumarray(mzDistance,1)));
c          = 0;
scanPointNewRoiIndex = zeros(0,1);
for (i = 1:length(count) - 1)
    if (count(i) == 1)
        c                  = c + 1;
        newRoI(c)          = newMZ(pos(i + 1));  %#ok<*AGROW>
        scanIntensities(c,:) = newMS(pos(i + 1),:);
        %         mzBinned(c,:) = newmzBinned(pos(i + 1),:);
    else
        nPts  = count(i) ;
        wL    = newMZ(pos(i) + 1) - mzerror/2:mzerror:newMZ(pos(i + 1)) + mzerror/3;
        wU    = wL + mzerror;
        mzLoc = newMZ(pos(i) + 1:pos(i + 1));
        iLoc  = newMS(pos(i) + 1:pos(i + 1),:);
        iLoc(isnan(iLoc))=0;
        
        %Only include mz's with intensity > threshold. The threshold is
        %calculated as a percentage of the the maximum peak within the scan
        %         iLoc(iLoc)=nan;
        %         mzLoc(iLoc)=nan;
        len   = length(wL);
        [scanPointRoiIndex,nPointsInROI] = basicBin(1,nPts,zeros(nPts,1),zeros(len,1),mzLoc,[],wL,wU);
        if sum(nPointsInROI==0)>0
            len= length(wL)-1;
        else
            len = length(wL);
        end
        if sum(scanPointRoiIndex==0)>0
            d= scanPointRoiIndex(scanPointRoiIndex>0);
            scanPointRoiIndex(length(d):end)=d(end); %%check that this is a valid way of doing it
        end
        newRoI(c + 1:c + len) = accumarray(scanPointRoiIndex,mzLoc) ./  nPointsInROI(1:len);
        
        newRoI(c + 1:c + len) = accumarray(scanPointRoiIndex,mzLoc) ./  nPointsInROI(1:len);
        
        scanPointNewRoiIndex  = cat(1,scanPointNewRoiIndex,scanPointRoiIndex + nroi + c);
        
        if size(iLoc,2)>1
            subs=[];
            for i=1:size(iLoc,1)
                v=1:size(iLoc,2);
                subs=cat(2,subs,v);
            end
            iLocInt=[];
            for i=1:size(iLoc,2)
                iLocInt=cat(1,iLocInt,iLoc(:,i));
            end
            if len>1
                u=unique(scanPointRoiIndex);
                for i=1:len
                    multiply=sum(scanPointRoiIndex==u(i));
                    scanIntensities(c + i,:) = accumarray(subs(1:size(iLoc,2)*multiply)',iLocInt(1:size(iLoc,2)*multiply)')./  nPointsInROI(i); %Calculates average;
                end
            else
                scanIntensities(c + 1:c + len,:) = accumarray(subs',iLocInt)'./  nPointsInROI(1:len); %Calculates average
            end
        else
            scanIntensities(c + 1:c + len,:) = accumarray(scanPointRoiIndex,iLoc) ./  nPointsInROI(1:len); %Calculates average;
        end
        c                              = c + len;
    end
    
    
end
end


function[mzroi,MSroi,mzBinned,ScanNumber] = CombTwoScans(mz_cell,Int_cell,mzerror,thresh,minroi,CheckEvery,s,e,nrows)

for irow=1:nrows
    fprintf(1,'irow: %i\n',irow);
    if irow==1
        mzOld  = mz_cell{irow};
        mzroi         = mz_cell{irow}';
        MSroi(:,1)    = Int_cell{irow}';
        mzBinned(:,1) = mz_cell{irow}';
        ScanNumber    = irow;
        
    else
        NewIntLoc=zeros(1,1);
        PosIntNew=zeros(1,1);
        NewLoc=zeros(1,1);
        PosNew=zeros(1,1);
        PosMZ=zeros(1,1);
        NewIntLoc=zeros(1,1);
        intLoc=zeros(1,1);
        mzLoc=zeros(1,1);
        
        mzOld=mzroi;
        mzroi=0;
        mzNew  = mz_cell{irow};
        IntNew = Int_cell{irow};
        
        %Calculates lower and upper boundary of the bin
        wL=mzOld-mzerror/2;
        wU=mzOld+mzerror/2;
        
        %resets counters to 1
        k=1;i=1;s=1;f=1; h=1;d=1;
        while i<=length(mzOld)
            while k <= length(mzNew) && s <=length(wU)%&& abs(N(k)-O(i))<=mzerror/2
                if mzNew(k)>wU(s)
                    f           = 1;
                    mzLoc(i,f)  = mzOld(i);
                    PosMZ(i)    = h;
                    intLoc(i,f) = 0;
                    h=h+1;
                    i          = i+1;
                    s          = s+1;
                    continue
                    
                end
                if mzNew(k)>=wL(s)
                    f           = f+1;
                    mzLoc(i,f)  = mzNew(k);
                    intLoc(i,f) = IntNew(k);
                    PosMZ(i)    = h;
                    
                else
                    NewLoc(d)    = mzNew(k);
                    NewIntLoc(d) = IntNew(k);
                    PosNew(d)       = h;
                    h =h+1;
                    d=d+1;
                end
                k=k+1;
             
            end
      
            
            if i<=length(mzOld)
                mzLoc(i,1)=mzOld(i);
                intLoc(i,1)=0;
                PosMZ(i) = h;
                h=h+1;
            end
            
            %Finds the ROIs in the new mz vector which were not present in the current ROI
            while k<=length(mzNew)
                NewLoc(d)    = mzNew(k);
                NewIntLoc(d) = IntNew(k);
                PosNew(d)    = h;
                h=h+1;
                d            = d+1;
                k            = k+1;
            end
            i=i+1;
            
        end
        
        % calculates the mean of the mz value across the two scans and int within
        % the scan
        
        % calculates the mean of the mz value across the two scans and int within
        % the scan
       
        mzLoc(mzLoc==0)=nan;
        intLoc(intLoc==0)=nan;
        
        mzroi(PosMZ)=mean(mzLoc,2,'omitnan');
        mzroi(PosNew(PosNew>0))=NewLoc(NewLoc>0)';
     
        dif=length(mzroi)-length(MSroi(:,irow-1));
        
                if dif>=0
                    mzBinned=cat(1,mzBinned,nan(dif,size(mzBinned,2)));
%                     mzBinned=cat(2,mzBinned,mzBinnedOld);
                    MSroi=cat(1,MSroi,zeros(dif,size(MSroi,2)));
%                     MSroi(:,irow)=intLoc;
                end
                
        % MSroi
        MSroi(PosMZ,irow)=mean(intLoc,2,'omitnan');
        
        MSroi(PosNew(PosNew>0),irow)=NewIntLoc(NewIntLoc>0)';
        
%         %mzBinned

%         mzBinned(PosMZ,:)=mzBinned;
%         g=size(mzBinned,2);
%         mzBinned(PosMZ,g+1:g+size(mzLoc,2)-1)=mzLoc(:,2:end);
%         mzBinned(PosNew(PosNew>0),g+1) = NewLoc(NewLoc>0)';
        %         intLoc=[mean(intLoc,2,'omitnan');NewIntLoc(NewIntLoc>0)'];
        ScanNumber=cat(2,ScanNumber,repelem(irow,size(mzLoc,2)));
        
        if isempty(NewLoc(NewLoc>0))
            mzBinnedOld=mzLoc;
        else
            mzBinnedOld=cat(1,mzLoc,[NewLoc(NewLoc>0)',nan(length(NewLoc(NewLoc>0)'),size(mzLoc,2)-1)]);
        end
        mzroi=[mean(mzLoc,2,'omitnan');NewLoc(NewLoc>0)'];
        
        nroi=0;
        %
        %processNewMZ(newMZ,newMS,thresh,mzerror,nroi)
        
            if  sum((mzroi(2:end)-mzroi(1:end-1))<0) > 0
            sortMatrix=sortrows(cat(2,mzroi,MSroi,mzBinned));
            mzroi=sortMatrix(:,1);
            MSroi=sortMatrix(:,2:size(mzroi,2)+size(MSroi,2));
            mzBinned=sortMatrix(:,size(mzroi,2)+size(MSroi,2)+1:end);
        end
    end
    mzBinned(mzBinned==0)=nan;
    
    % filters out mz's with less consecutive point than minroi
    if sum(irow==[minroi*2:CheckEvery:e-s])>0
        %    sortMatrix=sortrows(cat(2,mzroi,MSroi,mzBinned));
        %     mzroi=sortMatrix(:,1);
        %     MSroi=sortMatrix(:,2:size(mzroi,2)+size(MSroi,2));
        %     mzBinned=sortMatrix(:,size(mzroi,2)+size(MSroi,2)+1:end);
        [MSroi,mzroi,mzBinned]=MinROICheck(mzroi,MSroi,mzBinned,minroi);
        
    end
    %Output
    %     sortMatrix=sortrows(cat(2,mzroi,MSroi,mzBinned));
    %     mzroi=sortMatrix(:,1);
    %     MSroi=sortMatrix(:,2:size(mzroi,2)+size(MSroi,2));
    %     mzBinned=sortMatrix(:,size(mzroi,2)+size(MSroi,2)+1:end);
    %    mzBinned(mzBinned==0)=nan;
end

end