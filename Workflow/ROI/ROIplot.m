function ROIplot(roicelln)
% function ROIplot(roicelln)
% plot roicell content
% roicelln has the values to plot (output from ROIpeaks)
% It plots the mass traces at every selected ROI and
% the time profile for this ROI 
% e.g. ROIplot(roicell), it plots one by one all roi cells in roicell
% e.g. ROIplot(roicell(13,:)), it plots one specific roi dell 
% 

[nroi,dummy]=size(roicelln);

for i=1:nroi
    disp('roicell number: ');disp(i)
    roicell=roicelln(i,:);
    x=roicell{1,2};
    y=roicell{1,1};
    z=roicell{1,3};
    my=roicell{1,5};
    sdy=std(y);
    figure(1)
    plot(x,y,'ok')
    hold on
    plot([x(1),x(end)],[my,my],'-')
    plot([x(1),x(end)],[my+sdy,my+sdy],'--g')
    plot([x(1),x(end)],[my-sdy,my-sdy],'--g')
    plot([x(1),x(end)],[my+2*sdy,my+2*sdy],'--r')
    plot([x(1),x(end)],[my-2*sdy,my-2*sdy],'--r')
    tmy=num2str(my);tsdy=num2str(sdy);
    title(['mass trace at ROI value of: ',tmy,' +/- ',tsdy])
    xlabel('time in sec.')
    ylabel('m/z')
    hold off
    
    figure(2)
    plot(x,z,'or')
    hold on
    plot(x,z)
    title(['chromatographic peak at at ROI value of: ',tmy])
    xlabel('time in sec.')
    ylabel('MS intensity')
    pause
    hold off
end

end

