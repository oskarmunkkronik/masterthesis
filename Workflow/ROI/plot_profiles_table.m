function [area,height,table1,table2]=plot_profiles_table(x,c,s,time,mz,nexp,ncontrol)
% [area,height,table,table2]=plot_profiles_table(x,c,s,time,mz,nexp,ncontrol)
% % Plot and evaluation of concentration (c) and spectra (s) profiles  
% % with time and mz axes respectively 
% % e.g. from results of MCR-ALS after the analysis of data martrix x
% Explained variances are given for each individual component,
% for their sum and for the total explained variance when considered simultaneously
% when nexp=1, all profiles are in the same experiment, no statistical test is performed 
% when nexp > 1, areas/heights for each component are claculated separately for
% every experiment, and statistical tests are performed to check for differences
% between control and treated samples
% INPUT
% x is the data matrix used to give  explained variances (column-wise augmented or not))
% c is the concentration profiles matrix (column-wise augmented or not)
% s, is the spectra profiles matrix
% time, is the time axis of the concentration profiles matrix (augmented or not) 
% mz, is the mz axis of the spectra profiles
% nexp gives the total number of experiments simultaneously considered
% control and with treatment)
% ncontrol is the number of control samples (they should be the located 
% on the top of the augmentred concentration matrix
%
% OUTPUT
% gives areas and heights of c profiles for all components in each experiment, 
% table and table2 are the statistical results in tabular form (
% In table 2, only the components with differences statistically significant are given
% (including fold change and direction)
% [area,height,table,table2]=plot_profiles_table(x,c,s,time,mz,nexp,ncontrol)


%inizializations
[nr1,nc1]=size(c);
[nr2,nc2]=size(s);
nr=nr1/nexp;
close all
cg=get(gca,'ColorOrder');
r2t=0;
ntest=nexp-ncontrol;
table1(1:nc1,1:10)=zeros(nc1,10);
table2=table1;
it=0;

% Analysis and plot of c and s profiles
% for i=1:nc1
i=1;comp=1;
while comp < nc1+1
    disp(' ')
    % default changes are up (1), i.e treated > control; 
    % -1 means down control > treated
    changearea=1;
    changeheight=1;
    disp(['Analysis of profiles for component: ',num2str(i)])
    % variance evaluation
    [r2,sigma]=lof(x,c(:,i),s(i,:));
    
    % plot of c profiles
    ii=rem(i,7);
    figure(1)
    if ii == 0, ii=7; end
    [maxi,maxt]=max(c(:,i));
    area(i,1)=sum(c(:,i));
    height(i,1)=max(c(:,i));
    maxt=time(maxt);
    subplot(3,1,1)
    plot(c(:,i),'color',cg(ii,:))
    title(['elution profile of component: ',num2str(i),' by scan number']);
    subplot(3,1,2)
    plot(time,c(:,i),'color',cg(ii,:))
    ylim([0,maxi+0.05*maxi]);
    text(10,maxi-0.05*maxi,['peak max at: ',num2str(maxt),' sec']);
    title(['elution profile of component: ',num2str(i),' by time in sec']);
    
    % plot of s profiles
    [maxi,maxmz]=max(s(i,:));
    maxmz=mz(maxmz);
    subplot(3,1,3),
    % plot(mz,s(i,:)','color',cg(ii,:)) % in case of line plot preferred  
    bar(mz,s(i,:)'),colormap(cg(ii,:))
    ylim([0,maxi+0.05*maxi]);
    text(maxmz+0.01*maxmz,maxi-0.02*maxi,[num2str(maxmz),' m/z'])
    title(['pure spectrum of component: ',num2str(i)]);
    xlabel(['expl. var. by this component is (in %): ',num2str(r2)])
    r2t=r2t+r2;
    
      
    % evaluation of the mean areas and heights
    meanarea(1)=mean(area(i,1));
    meanheight(1)=mean(height(i,1));
    disp(['time, mz:  ',num2str([maxt,maxmz])])
    disp(['area, height:  ',num2str([area(i,1),height(i,1)])])
    
    % in case of multiple experiments (control and treated samples)
    % i.e. for column-wise augmented c matrix
    if nexp>1
        nrend=0;
        for j=1:nexp
            nrinic=nrend+1;
            nrend=nrend+nr;
            area(i,j)=sum(c(nrinic:nrend,i));
            height(i,j)=max(c(nrinic:nrend,i));
        end
        % disp(' ')
        disp(['areas: ',num2str(area(i,:))])
        % disp(' ')
        disp(['heights ',num2str(height(i,:))])
       
        figure(2)
        subplot(2,1,1)
        bar(area(i,:)'),colormap(cg(ii,:))
        title(['profile area of component: ',num2str(i),' for the ',num2str(nexp),' experiments']);
        subplot(2,1,2)
        bar(height(i,:)'),colormap(cg(ii,:))
        title(['profile max height of component: ',num2str(i),' for the ',num2str(nexp),' experiments']);
        % Perform statistical test on the means and significance evaluation
        % (using Statistics tollbox of MATLAB)
        meanarea(1)=mean(area(i,1:ncontrol));
        meanarea(2)=mean(area(i,ncontrol+1:nexp));
        ratioarea=meanarea(2)/meanarea(1);
        if ratioarea<1,
            ratioarea=1/ratioarea;
            changearea=-1;
        end
        meanheight(1)=mean(height(i,1:ncontrol));
        meanheight(2)=mean(height(i,ncontrol+1:nexp));
        ratioheight=meanheight(2)/meanheight(1);
        if ratioheight<1,
            ratioheight=1/ratioheight;
            changeheight=-1;
        end
        disp(['mean areas in control and treated samples are: ',num2str([meanarea(1),meanarea(2)])]);
        disp(['areas fold change is: ',num2str([changearea,ratioarea])]); 
        disp(['mean heights in control and treated samples are: ',num2str([meanheight(1),meanheight(2)])]);
        disp(['height fold change is: ',num2str([changeheight,ratioheight])]); 
        if ncontrol==ntest,
            [HA,PA] = ttest(area(i,1:ncontrol),area(i,ncontrol+1:nexp));
            [HH,PH] = ttest(height(i,1:ncontrol),height(i,ncontrol+1:nexp));
            [HA2,PA2] = ttest2(area(i,1:ncontrol),area(i,ncontrol+1:nexp));
            [HH2,PH2] = ttest2(height(i,1:ncontrol),height(i,ncontrol+1:nexp));
            [PA3,HA3] = ranksum(area(i,1:ncontrol),area(i,ncontrol+1:nexp));
            [PH3,HH3] = ranksum(height(i,1:ncontrol),height(i,ncontrol+1:nexp));
            disp('paired t-test results')
            disp(['H = ',num2str(HA),' diff in peak areas control/test samples  (y/n 1/0) with p significance: ',num2str(PA)]) 
            disp(['H = ',num2str(HH),' diff in peak heights control/test samples(y/n 1/0) with p significance: ',num2str(PH)])  
            disp('two-samples t-test results')
            disp(['H = ',num2str(HA2),' diff in peak areas control/test samples  (y/n 1/0) with p significance: ',num2str(PA2)]) 
            disp(['H = ',num2str(HH2),' diff in peak heights control/test samples(y/n 1/0) with p significance: ',num2str(PH2)])  
            disp('rank-sum test results')
            disp(['H = ',num2str(HA3),' diff in peak areas control/test samples  (y/n 1/0) with p significance: ',num2str(PA3)]) 
            disp(['H = ',num2str(HH3),' diff in peak heights control/test samples(y/n 1/0) with p significance: ',num2str(PH3)])  
        else
            [HA2,PA2] = ttest2(area(i,1:ncontrol),area(i,ncontrol+1:nexp));
            [HH2,PH2] = ttest2(height(i,1:ncontrol),height(i,ncontrol+1:nexp));
            [PA3,HA3] = ranksum(area(i,1:ncontrol),area(i,ncontrol+1:nexp));
            [PH3,HH3] = ranksum(height(i,1:ncontrol),height(i,ncontrol+1:nexp));
            disp('two-samples t-test results')
            disp(['H = ',num2str(HA2),' diff in peak areas control/test samples  (y/n 1/0) with p significance: ',num2str(PA2)]) 
            disp(['H = ',num2str(HH2),' diff in peak heights control/test samples(y/n 1/0) with p significance: ',num2str(PH2)])
            disp('rank-sum test results')
            disp(['H = ',num2str(HA3),' diff in peak areas control/test samples  (y/n 1/0) with p significance: ',num2str(PA3)]) 
            disp(['H = ',num2str(HH3),' diff in peak heights control/test samples(y/n 1/0) with p significance: ',num2str(PH3)])  
        end
        
   %Create a table

         
         table1(i,1)=i;
         table1(i,2)=r2;
         table1(i,3)=maxmz;
         table1(i,4)=maxt;
         table1(i,5)=meanarea(1);
         table1(i,6)=meanarea(2);
         table1(i,7)=meanheight(1);
         table1(i,8)=meanheight(2);
         table1(i,9)=PA2;
         table1(i,10)=PH2;
         table1(i,11)=ratioarea;
         table1(i,12)=ratioheight;
         table1(i,13)=changearea;
         table1(i,14)=changeheight;
         if PA2 && PH2 <= 0.05;
             it=it+1;
             table2(it,1)=i;
             table2(it,2)=r2;
             table2(it,3)=maxmz;
             table2(it,4)=maxt;
             table2(it,5)=meanarea(1);
             table2(it,6)=meanarea(2);
             table2(it,7)=meanheight(1);
             table2(it,8)=meanheight(2);
             table2(it,9)=PA2;
             table2(it,10)=PH2;
             table2(it,11)=ratioarea;
             table2(it,12)=ratioheight;
             table2(it,13)=changearea;
             table2(it,14)=changeheight;
         end
         
     
     
    else
         table1(i,1)=i;
         table1(i,2)=r2;
         table1(i,3)=maxmz;
         table1(i,4)=maxt;
         table1(i,5)=meanarea(1);
         table1(i,6)=meanheight(1);
         table1(i,7)=1;
         table1(i,8)=1;
    end

    % Go for or back 
    disp(' ')
    str=input('select forward/backward plot 1/0 ');
    if str==0,
        i=i-1;
        comp=comp-1;
    else
        i=i+1;
        comp=comp+1;
    end
      
end

% Display final results 

disp(' '),disp(' '),disp('**************************************')
disp('% total explained variance / % lack of fit fit')
[r2,sigma]=lof(x,c,s);
disp(' ')
disp(['% total sum of individual explained variances is:  ',num2str(r2t)]);
disp(['% total overlapped variance is: ',num2str(r2t-r2)]);
disp('click one key to see final table of results')
pause
disp(' '),disp(' '),disp('*****************SUMMARY OF RESULTS*********************')
disp(table1)
if nexp>1
    disp(' '),disp(' '),disp('***************** RESULTS FOR ONLY SIGNIFICATIVE CONCN CHANGES *********************')
    disp(table2)
end
end

