% %%
function [CleanMSroi,Cleanmzroi,CleanmzBinned]=MinROICheck(mzroi,MSroi,mzBinned,minroi)

CleanMSroi   = MSroi;
Cleanmzroi   = mzroi;
CleanmzBinned= mzBinned;
RemRows=repelem(true,size(MSroi,1));
    V = MSroi>0;
    for j =1:size(MSroi,1)
          i=1;    d=0;

    while minroi>=d && i<=size(V,2)-1
        if V(j,i)>0
            d = d+1;
            if d>=minroi
                RemRows(j)=false;
            end
        else
            d=0;
        end
        i=i+1;
    end
    end 
        CleanMSroi(RemRows,:)=[];
        Cleanmzroi(RemRows,:)=[];
        CleanmzBinned(RemRows,:)=[];
end 