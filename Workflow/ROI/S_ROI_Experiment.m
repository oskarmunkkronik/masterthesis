%%
%ROI experiment Optimization

thresh=[0.1,0.5,1,3];
massaccuracy=8*10^-6;% in ppm
multiple=[10,50,200];
mzerror=800*massaccuracy*multiple;% in Da measured a 800 Da
ScanTime=0.35; %Seconds pr. scan
MinPeakWidth=[4,6,8]; %Seconds
minroi=round(MinPeakWidth/ScanTime);

%Creates data table
ROI_Experiments=table('Size',[36 7],'VariableTypes',repmat({'double'},1,7),'VariableNames',{'Thresh','mzerror','PeakWidth','Variance','Var (%Maintained)','#Entries','Var (%Maintained)/#Entries'});
mzroi01=cell(36,1);
MSroi01=cell(36,1);
roicell01=cell(36,1);
I=1;
for (T=1:size(thresh,2))
        for (M=1:size(mzerror,2))
            for (W = 1:size(minroi,2))
                ROI_Experiments{I,1}=thresh(T);
                ROI_Experiments{I,2}=mzerror(M);
                ROI_Experiments{I,3}=minroi(W);
                
                I=I+1;
            end
        end 
end
for (f = 4) % length(fileList))
    
    fprintf('File %i/%i\n',f,length(fileList01))
    A= cdf2struct(fileList01(f).name,{'intensity_values','mass_values','point_count','scan_acquisition_time'})  ;
    
    %Organizing data for ROI function.
    %peaks is a cell array with nrows= number of scans. In each cell
    %the mz values, mz intensities measured in the scan is organized
    %[mz_Values mz_Intensity]
   
    
    parfor (I=1:size(ROI_Experiments,1))
      
               
                peaks=cell(A.point_count.size,1);
                k1=1;
                nrows=A.point_count.size;
                for (S = 1:A.point_count.size)
                    k2=k1+A.point_count.data(S)-1;
                    peaks{S,1}=[A.mass_values.data(k1:k2),A.intensity_values.data(k1:k2)];
                    k1 = A.point_count.data(S)+k1;
                end
                
                [mzroi01{I},MSroi01{I},roicell01{I}]=ROIpeaks(peaks,ROI_Experiments{I,1},ROI_Experiments{I,2},ROI_Experiments{I,3},nrows,A.scan_acquisition_time.data);
                
            end
        end
    
for I = 1:36
    ROI_Experiments{I,4}=var(MSroi01{I},0,'all');
ROI_Experiments{I,5}=round(ROI_Experiments{I,4}/ROI_Experiments{1,4}*100,2);
ROI_Experiments{I,6}=size(mzroi01{I},2);
ROI_Experiments{I,7}=round(ROI_Experiments{I,5}/ROI_Experiments{I,6},2);

end
ROI_Experiments
