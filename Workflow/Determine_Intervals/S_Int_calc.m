%Determine intervals: 
%%
addpath 'L:\MasterThesis\MatLab\Workflow\Determine_Intervals'
%Calculates intervals
    % x: The vector on which the savitsky-golay needs to be calculated
    % size(1 x Number of Scans
    % m: number of filtered points (FIR order)
    % M: output point , M = (m+1)/2; % middle point for odd m
    % n: approximation polynomial order
   
x=mean(max(X,[],1),3);
size(x)
n=2;
m=3;
MinInt=20; 
Intervals=zeros(1,2);
[Intervals]=Int_calc(x,m,n,MinInt);
% figure

size(Intervals)
 figure
 plot(Intervals(:,2)-Intervals(:,1),'o')
 ylabel('Interval length in scans')
 title('Interval length')
 xlabel('Interval number')
set(gca,'FontSize',25)
% % 

%%
close all
figure
% subplot(2,1,1)

BPC=max(X,[],1);
I
for k =1:size(X,3)
    hold on 
plot(s:e,BPC(1,s:e,k))
k
end
% plot(dy_est)
grid on
axis tight

for k=1:size(Intervals,1)
    if(Intervals(k,1) > s && Intervals(k,2)<e)
        hold on
    xline(Intervals(k,1))
    xline(Intervals(k,2),'Color','r')
    end 
end 

%%
xlabel('Rt (scans)')
ylabel('Int (a.u.)')
subplot(2,1,2)

yline(0)
hold on
plot(1:2020,mean(X_dif,2)','LineWidth',3,'Color','r')
plot(1:2020,mean(X_dif,2),'bo','MarkerSize',10)
linkaxes
% plot(X_dif)
for k=1:size(Intervals,1)
    hold on
    xline(Intervals(k,1))
    xline(Intervals(k,2),'Color','r')
end 


%%
close all
figure 

S=1;
Int1=20;
for (Int=Int1:Int1+15)
 
    subplot(4,4,S)
    plot(max(X(Intervals(Int,1):Intervals(Int,2),:),[],2))
    hold on
      S=1+S;
      xticklabels(Intervals(Int,1):2:Intervals(Int,2))
end 

%%
figure
plot(Intervals(:,2)-Intervals(:,1),'o')
yline(mean(Intervals(:,2)-Intervals(:,1)))
