
function [Int2]=Int_calc(x,m,n,MinInt)
%Input parameters

%x: The vector on which the savitsky-golay needs to be calculated
% m number of filtered points (FIR order)
% M output point , M = (m+1)/2; % middle point for odd m
% n  approximation polynomial order
h = 1; %Sample step (distance between sampling spots)

coef=SG_calc(m,n,h);
dy_est  = filter(coef(2,:),1,x);


Int=zeros(1,2);

k=1;
I=1;

while k<size(dy_est,2)-1
    Int(I,1)=k;
    y1=sign(dy_est(1,k));
    
    while size(dy_est,2)>k && y1==sign(dy_est(1,k))
        k=k+1;
    end
    k0=k;
    y1=sign(dy_est(1,k));

 
    while  size(dy_est,2)>k && y1==sign(dy_est(1,k)) 
        k=k+1;
    end
%     
%     if k<size(dy_est,2)-size(p,2)
%         
%         [~,d]=min(abs(dy_est(1,k:k+size(p,2)-1)));
%         
%         Int(I,2)=k+p(d);
%     else
%         Int(I,2)=k+p(d);
%     end
    %k=Int(I,2)+1;
    Int(I,2)=k;
    I=I+1;
end

Int2=zeros(1,2);
k=1;
k1=1;
I=1;
Int_Temp=sum(Int(k:k1,2)-Int(k:k1,1));
while k<size(Int,1)-1
while MinInt>Int_Temp
  k1=k1+1;
    Int_Temp=sum(Int(k:k1,2)-Int(k:k1,1));
      
end 

Int2(I,:)=[Int(k,1),Int(k1,2)];
I=I+1;
k=k1+1;
k1=k;
Int_Temp=0;
end 
Int2(end,2)=size(x,2);
end 


