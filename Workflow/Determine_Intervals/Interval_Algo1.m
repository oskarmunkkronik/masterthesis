close all
function [Int]=Int_calc(x,start,final,m,n)
%Input parameters

%x: The vector on which the savitsky-golay needs to be calculated
% m = 5;      % number of filtered points (FIR order)
% M = (m+1)/2;      % output point , M = (m+1)/2; % middle point for odd m
% n = 2;      % approximation polynomial order

h = 1; %Sample step (distance between sampling spots)  

coef=SG_calc(m,n,h);
dy_est  = filter(coef(2,:),1,x);


Int=zeros(1,1);

k=1;
I=1;
while k<size(dy_est,2)-1
Int(I,1)=k;
    y1=sign(dy_est(1,k));
   
    while size(dy_est,2)>k && y1==sign(dy_est(1,k))
    k=k+1;
    end
    
      y1=sign(dy_est(1,k));
    
 
    while  size(dy_est,2)>k &&y1==sign(dy_est(1,k)) 
        
    k=k+1;
    
    k
    end 
    
         Int(I,2)=k
    
    I=I+1;
end 
figure

for k = 1:size(Xtic,1)
plot(stat:final,x)
hold on 
end 
hold on 

plot(dy_est)
grid on
axis tight

for k=1:size(Int,1)
    xline(Int(k,1))
    xline(Int(k,2))
end 
end
