addpath L:\MasterThesis\DataFiles\CSV
CompInChrom=readtable("Compounds_in_chromatograhic_run_QC",'PreserveVariableNames',true);
%%
x=CompInChrom{:,3};
histogram(x,1000)
xticks([0:1:17])
xticklabels([0:1:17])
xlabel('Rt (min)')
ylabel('No. Candidate Masses')
title('Histogram of candidate masses')
grid on 
axis tight
set(gca,'FontSize',25)