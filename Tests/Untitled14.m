% cd 'L:\MasterThesis\DataFiles\NetCDF'
% addpath 'L:\MasterThesis\MatLab\Workflow\Import_Data\chromCDF'
% 
% addpath L:\MasterThesis\MatLab\Workflow\ROI
% addpath C:\Users\mht541\Documents\ChromTools
% % Loading MS trace
fileList01  = dir('*01.CDF');
fileList02  = dir('*02.CDF');
I=1;
clear X
peaks=cell(A.point_count.size,6);
for (f=71)
    fprintf('File %i/%i\n',f,length(fileList01))
    A= cdf2struct(fileList01(f).name,{'intensity_values','mass_values','point_count','scan_acquisition_time'})  ;
    %
    k1=1;
    nrows=A.point_count.size;
    for (S = 1:A.point_count.size)
        
        k2=k1-1+A.point_count.data(S);
        peaks{S,I}=[A.mass_values.data(k1:k2),A.intensity_values.data(k1:k2)];
        k1 = A.point_count.data(S)+k1;
        G(S)=size(peaks{S}(:,:),1);
    end 
%    [chromX01{f},mz01{f}] = binVec2(A.intensity_values.data,A.mass_values.data,A.point_count.data,1,[1 0 0.1]);
%     
%     
%     X(:,:,I)=chromX01{f}(1:2020,:);
%   I=I+1;
    end
% % % sum(G' ~= A.point_count.data,'all')
% % % %
% % % figure
% % % plot(G)
% % % hold on 
% % % plot( A.point_count.data,'--r')
% % % sum(G)-sum(A.point_count.data)
% % % %
% 
%    
%     1
    %%
ROIsamples=cell(5,1);
thresh=500;
massaccuracy=8*10^-6;% in ppm
multiple=3;
mzerror=0.02;%800*massaccuracy*multiple;% in Da measured a 800 Da
ScanTime=0.72; %Seconds pr. scan
MinPeakWidth=5; %Seconds
Int=2020;
s=1%Intervals(19,1);
e=500%Intervals(21,2);
nrows=2020;
CheckEvery=10;
minroi=6;
tic
% [mzroi,MSroi,roicell]=ROIpeaks(peaks,thresh,mzerror,minroi,nrows,time)  
f=1;
for (f=1)
     fprintf(1,'Sample: %i\n',f);
%  tic
% [mzroi,MSroi,mzBinned,ScanNumber,roicell]=ROIpeaks(peaks(:,f),thresh,mzerror,minroi,CheckEvery,s,e,nrows,A.scan_acquisition_time.data);
% b(1)=toc
tic
[mzroi,MSroi,roicell]=ROIpeaks_Barcelona(peaks(:,f),thresh,mzerror,minroi,nrows,A.scan_acquisition_time.data);
b(2)=toc
data.mzroi=mzroi;
data.MSroi=MSroi;
data.mzBinned=mzBinned;
ROIsamples{f}=data;
end 

%%
tic
mzerror2= 0.5;
[mzroi,MSroi,mzBinned] = CombineTwoSamples(ROIsamples,mzerror2);
 
toc

% roicell

% %%2020
% % close all
% figure
% 
% hold on 
% n=nrows;
% title(num2str(nrows))
% % plot(roicell{1,n},roicell {2,n}./max(roicell {2,n}),'og')
% % ROI=plot(mzroi,MSroi(:,n)./max(max(MSroi(:,n),[],1)),'-<r');
% % Bin=plot(mz01{f},X(:,n)./max(max(X(:,n),[],1)),'--b');
% % RawData=plot(peaks{n}(:,1),peaks{n}(:,2)./max(peaks{n}(:,2)),'x');
% % legend([ROI,Bin,RawData],{"ROI","Bin",'RawData'})
% % 
% %%
% figure
% D=MSroi'./sum(MSroi','all','omitnan');
% D(isnan(D))=0;
% plot(D,'r')
% % hold on
% % 
% % % D2=MSroi2'./sum(MSroi2','all','omitnan');
% % % D2(isnan(D2))=0;
% % % plot(D2','--g')
% % hold on
% % plot((X(1:nrows,:)./sum(X(1:nrows,:),'all'))','--b')