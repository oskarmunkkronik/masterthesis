mzerror=0.4
roicell{1,1}=[1:0.5:3]
roicell{1,2}=[0.1,2,5:6]
roicell{2,1}=[1:0.5:3]
roicell{2,2}=[4:7]
[mzroi,MSroi,mzBinned] = CombTwoScans(roicell(1,:),roicell(2,:),mzerror,thresh,minroi,3);
size(mzroi)
