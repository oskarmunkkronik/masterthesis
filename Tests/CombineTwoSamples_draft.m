
function[mzroi,MSroi,mzBinned,ScanNumber] = CombTwoScans(mz_cell,Int_cell,mzerror,thresh,minroi,CheckEvery,s,e,nrows)

for irow=1:nrows
    fprintf(1,'irow: %i\n',irow);
    if irow==1
        mzroi         = mz_cell{irow}';
        MSroi(:,1)    = Int_cell{irow}';
        mzBinned(:,1) = mz_cell{irow}';
        ScanNumber    = irow;
     
    else
        %Initialze fucntion
        PosNew=zeros(1,1);
        PosMZOld=zeros(1,1);
        PosMZNew=zeros(1,1);
        PosMZ=zeros(1,1);
        PosInVecNew=zeros(length(mzNew),1);
        PosInVecOld=zeros(length(mzroi),1);
        NMeasurementsInBin=ones(size(mzroi,1),1);
        NewIntLoc=nan(1,1);
        intLoc=nan(1,1);
        mzLoc=nan(1,1);

        %New data 
        mzOld=mzroi;
        mzNew  = mz_cell{irow};
        IntNew = Int_cell{irow};
        
        %Calculates lower and upper boundary of the bin
        wL=mzOld-mzerror/2;
        wU=mzOld+mzerror/2;
        
        %resets counters to 1
        k=1;i=1;s=1;f=1; h=1;d=1;
        while i<=length(mzOld)
            while k <= length(mzNew)&& s<=length(wU)
           
                if mzNew(k)>wU(s)
                    f           = 1;
%                     mzLoc(i,f)  = mzOld(i);
                    PosMZOld(i)    = h;
                    PosInVecOld(i)  = h;
                   
                    NMeasurementsInBin(i) = f;
                     NuMmzBinned(k)=f;
%                     intLoc(i,f) = 0;
                     h=h+1;
                    i          = i+1;
                    s          = s+1;
                    continue
                   
                end
              
                if mzNew(k)>=wL(s)
                    f           = f+1;
%                     mzLoc(i,f)  = mzNew(k);
                    PosInVecNew(k)= h;
                     NuMmzBinned(k)=f;
                    intLoc(i,f) = IntNew(k);   
%                     PosMZNew(h)    = h;
                    NMeasurementsInBin(h) = f;
                else
                   
%                     NewLoc(d)    = mzNew(k);
%                     NewIntLoc(d) = IntNew(k);
                    PosNew(d)       = h;
                    PosInVecNew(k)    = h;
                    NMeasurementsInBin(h) = 1;
                     NuMmzBinned(k)=f;
                    h =h+1;
                    d=d+1;
                end
                k=k+1;
            end
         
            if i<=length(mzOld)
%                 mzLoc(i,1)=mzOld(i);
%                 intLoc(i,1)=0;
                PosMZOld(i) = h;
                PosInVecOld(i)  = h;
                NMeasurementsInBin(h) = 1;
            h=h+1;
            end
            
            %Finds the ROIs in the new mz vector which were not present in the current ROI
            while k<=length(mzNew)
%                 NewLoc(d)    = mzNew(k);
%                 NewIntLoc(d) = IntNew(k);
                PosInVecNew(k)  = h;
                PosNew(d)    = h;
                 NMeasurementsInBin(h) = 1;
                  NuMmzBinned(k)=f;
                h=h+1;

                d            = d+1;
                k            = k+1;
               
            end
            i=i+1;
        end
% clear D
mzroi=accumarray([PosInVecNew;PosInVecOld],[mzNew,mzOld]')'./NMeasurementsInBin;
len=max(PosInVecNew);
MSroi(PosInVecOld,:)=MSroi;
Int_Temp=accumarray(PosInVecNew,IntNew')'./NMeasurementsInBin(1:len);
MSroi(PosInVecNew,irow)=Int_Temp(Int_Temp>0);
mzBinned(PosInVecOld,:)=mzBinned;
% % mzBinned(PosInVecNew,:)=

diff=max([PosInVecNew;PosInVecOld])-size(mzBinned,1);
if size(mzBinned,1)<max([PosInVecNew;PosInVecOld])
    mzBinned=cat(1,mzBinned,zeros(diff,size(mzBinned,2)));
end 
mzBinned=cat(2,mzBinned,zeros(size(mzBinned,1),max(NuMmzBinned)));

for ii=1:length(PosInVecNew)
mzBinned(PosInVecNew(ii),end-max(NuMmzBinned)+NuMmzBinned(ii))=mzNew(ii);
end
   % filters out mz's with less consecutive point than minroi
    if sum(irow==[minroi*2:CheckEvery:e-s])>0

    [MSroi,mzroi,mzBinned]=MinROICheck(mzroi,MSroi,mzBinned,minroi);

    end

    end
end 
end 
