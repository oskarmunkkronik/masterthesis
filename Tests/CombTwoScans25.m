
function[mzroi,MSroi,mzBinned,ScanNumber] = CombTwoScans25(mzroiNew,mzroiOld,IntNew,IntOld,mzerror)
%     fprintf(1,'irow: %i\n',nmzroi);
        NewLoc=zeros(1,1);
        PosNew=zeros(1,1);
        PosMZ=zeros(1,1);
        NewIntLoc=zeros(1,1);
        intLoc=zeros(1,1);
        
        mzLoc=zeros(size(mzroiOld,1),10);
        mzNew  = mzroiNew;

        %Calculates lower and upper boundary of the bin
        wL=mzroiOld-mzerror/2;
        wU=mzroiOld+mzerror/2;
        
        %resets counters to 1
        k=1;i=1;s=1;f=1; h=1;d=1;
        while i<=length(mzroiOld)
            while k <= length(mzNew) && s<=length(wU)&& i <=length(wU)%&& abs(N(k)-O(i))<=mzerror/2
                if mzNew(k)>wU(s)
                    f           = 1;
                    mzLoc(i,f)  = mzroiOld(i);
                    PosMZ(i)    = h;
                    intLoc(i,f) = 0;
                    h=h+1;
                    i          = i+1;
                    s          = s+1;
                    if 
                    continue
                    
                end
                if mzNew(k)>=wL(s)
                    f           = f+1;
                    mzLoc(i,f)  = mzNew(k);
                    intLoc(i,f) = IntNew(k);
                    PosMZ(i)    = h;
                    
                else
                    NewLoc(d)    = mzNew(k);
                    NewIntLoc(d) = IntNew(k);
                    PosNew(d)       = h;
                    h =h+1;
                    d=d+1;
                end
                k=k+1;
            end
            
            if i<=length(mzroiOld)
                mzLoc(i,1)=mzroiOld(i);
                intLoc(i,1)=0;
                PosMZ(i) = h;
                h=h+1;
            end
            
            %Finds the ROIs in the new mz vector which were not present in the current ROI
            while k<=length(mzNew)
                NewLoc(d)    = mzNew(k);
                NewIntLoc(d) = IntNew(k);
                PosNew(d)    = h;
                h=h+1;
                d            = d+1;
                k            = k+1;
            end
            i=i+1;
            
        end
        
        % calculates the mean of the mz value across the two scans and int within
        % the scan
        
        % calculates the mean of the mz value across the two scans and int within
        % the scan
        if  sum((mzroi(2:end)-mzroi(1:end-1))<0) > 0
            sortMatrix=sortrows(cat(2,mzroi,MSroi,mzBinned));
            mzroi=sortMatrix(:,1);
            MSroi=sortMatrix(:,2:size(mzroi,2)+size(MSroi,2));
            mzBinned=sortMatrix(:,size(mzroi,2)+size(MSroi,2)+1:end);
        end
        mzLoc(mzLoc==0)=nan;
        intLoc(intLoc==0)=nan;
        
        mzroi(PosMZ)=mean(mzLoc,2,'omitnan');
        mzroi(PosNew(PosNew>0))=NewLoc(NewLoc>0)';
        
        % MSroi
        MSroi(PosMZ,nmzroi)=mean(intLoc,2,'omitnan');
        
        MSroi(PosNew(PosNew>0),nmzroi)=NewIntLoc(NewIntLoc>0)';
        
        %mzBinned
        mzBinned(PosMZ,:)=mzBinned;
        g=size(mzBinned,2);
        mzBinned(PosMZ,g+1:g+size(mzLoc,2)-1)=mzLoc(:,2:end);
        mzBinned(PosNew(PosNew>0),g+1) = NewLoc(NewLoc>0)';
        %         intLoc=[mean(intLoc,2,'omitnan');NewIntLoc(NewIntLoc>0)'];
        ScanNumber=cat(2,ScanNumber,repelem(nmzroi,size(mzLoc,2)));
        
        if isempty(NewLoc(NewLoc>0))
            mzBinnedOld=mzLoc;
        else
            mzBinnedOld=cat(1,mzLoc,[NewLoc(NewLoc>0)',nan(length(NewLoc(NewLoc>0)'),size(mzLoc,2)-1)]);
        end
        mzroi=[mean(mzLoc,2,'omitnan');NewLoc(NewLoc>0)'];
        
        nroi=0;
        %
        %processNewMZ(newMZ,newMS,thresh,mzerror,nroi)
        
        
        dif=length(mzroi)-length(MSroi(:,nmzroi-1));
        
        %         if dif>=0
        %             mzBinned=cat(1,mzBinned,nan(dif,size(mzBinned,2)));
        %             mzBinned=cat(2,mzBinned,mzBinnedOld);
        %             MSroi=cat(1,MSroi,zeros(dif,size(MSroi,2)));
        %             MSroi(:,irow)=intLoc;
        %         end

    mzBinned(mzBinned==0)=nan;
    
    % filters out mz's with less consecutive point than minroi
    if sum(nmzroi==[minroi*2:CheckEvery:e-s])>0
        %    sortMatrix=sortrows(cat(2,mzroi,MSroi,mzBinned));
        %     mzroi=sortMatrix(:,1);
        %     MSroi=sortMatrix(:,2:size(mzroi,2)+size(MSroi,2));
        %     mzBinned=sortMatrix(:,size(mzroi,2)+size(MSroi,2)+1:end);
        [MSroi,mzroi,mzBinned]=MinROICheck(mzroi,MSroi,mzBinned,minroi);
        
    end
    %Output
    %     sortMatrix=sortrows(cat(2,mzroi,MSroi,mzBinned));
    %     mzroi=sortMatrix(:,1);
    %     MSroi=sortMatrix(:,2:size(mzroi,2)+size(MSroi,2));
    %     mzBinned=sortMatrix(:,size(mzroi,2)+size(MSroi,2)+1:end);
    %    mzBinned(mzBinned==0)=nan;
    end
    
