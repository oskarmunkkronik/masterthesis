
function[mzroi,MSroi,mzBinned] = CombTwoScans(mz_cell,Int_cell,mzerror,thresh,minroi,nrows)

for irow=1:nrows
    disp('irow=')
    irow
    if irow==1
        mzOld         = mz_cell{irow};
        mzroi         = mz_cell{irow}';
        MSroi(:,1)    = Int_cell{irow}';
        mzBinned(:,1) = mz_cell{irow}';
    else
        NewIntLoc=zeros(1,1);
        NewLoc=zeros(1,1);
        NewIntLoc=zeros(1,1);
        intLoc=zeros(1,1);
        mzLoc=zeros(1,1);
        
        mzOld=mzroi;
        mzNew  = mz_cell{irow};
        IntNew = Int_cell{irow};
        
        %Calculates lower and upper boundary of the bin
        wL=mzOld-mzerror/2;
        wU=mzOld+mzerror/2;
        
        %resets counters to 1
        k=1;i=1;s=1;f=1; h=1;d=1;
        while i<=length(mzOld)
            while k <= length(mzNew)&& s<=length(wU)&& i <=length(wU)%&& abs(N(k)-O(i))<=mzerror/2
                if mzNew(k)>wU(s)
                    f           = 1;
                    mzLoc(i,f)  = mzOld(i);
                    intLoc(i,f) = 0;
                    i          = i+1;
                    s          = s+1;
                    continue
                end
                if mzNew(k)>=wL(s)
                    f           = f+1;
                    mzLoc(i,f)  = mzNew(k);
                    intLoc(i,f) = IntNew(k);
                    
                    
                else
                    NewLoc(d)    = mzNew(k);
                    NewIntLoc(d) = IntNew(k);
                    d=d+1;
                end
                k=k+1;
            end
            
            if i<=length(mzOld)
                mzLoc(i,1)=mzOld(i);
                intLoc(i,1)=0;end
            %Finds the ROIs in the new mz vector which were not present in the current ROI
            while k<=length(mzNew)
                NewLoc(d)    = mzNew(k);
                NewIntLoc(d) = IntNew(k);
                d            = d+1;
                k            = k+1;
            end
            i=i+1;
        end
        
        % calculates the mean of the mz value across the two scans and int within
        % the scan
        
        % calculates the mean of the mz value across the two scans and int within
        % the scan
       
        mzLoc(mzLoc==0)=nan;
        intLoc(mzLoc==0)=nan;
        intLoc=[mean(intLoc,2,'omitnan');NewIntLoc(NewIntLoc>0)'];
        
        mzBinned=mzLoc(:,2:end);
        
        mzroi=[mean(mzLoc,2,'omitnan');NewLoc(NewLoc>0)'];
        
        nroi=0;
        %
        %processNewMZ(newMZ,newMS,thresh,mzerror,nroi)
        
        
        dif=length(mzroi)-length(MSroi(:,irow-1));
        
        if dif>=0
            mzBinned=cat(1,mzBinned,zeros(dif,size(mzBinned,2)));
            MSroi=cat(1,MSroi,zeros(dif,size(MSroi,2)));
            MSroi(:,irow)=intLoc;
            %             MSroi(:,irow)=cat(1,MSroi,zeros(size(MSroi,1),-dif));
        end
    end
    
    
    % filters out mz's with less consecutive point than minroi
    if sum(irow==[100:100:2020,2020])>0
        sortMatrix=sortrows(cat(2,mzroi,MSroi));
        mzroi=sortMatrix(:,1);
        MSroi=sortMatrix(:,2:end);
        mzBinned=sortrows(mzBinned);
        [MSroi,mzroi,mzBinned]=MinROICheck(mzroi,MSroi,mzBinned,minroi);
        
        [mzroi,~,MSroi] = processNewMZ2(mzroi,MSroi,1,mzerror,nroi);
        mzroi=mzroi';
    end
    %Output
    sortMatrix=sortrows(cat(2,mzroi,MSroi));
    mzroi=sortMatrix(:,1);
    MSroi=sortMatrix(:,2:end);
    mzBinned=sortrows(mzBinned);
    nroi=0;
    
    %Bins the output so that the ROIs are separated by more than mzerror/2
    
    
end

end