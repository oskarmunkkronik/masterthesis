function [newRoI,scanPointNewRoiIndex,scanIntensities] = processNewMZ2(newMZ,newMS,~,mzerror,nroi)
newRoI               = [];
scanIntensities      = [];
scanPointNewRoiIndex = [];
newmzBinned=[];
mzDistance = (newMZ(2:end) - newMZ(1:end - 1)) <= mzerror/2;
mzDistance = -diff([false;mzDistance;false]); % FIXME: the next line is simpler if there is no minus
mzDistance = cumsum(cat(1,1,cumsum(mzDistance) + 1));
mzDistance = mzDistance(1:end - 1);
count      = accumarray(mzDistance,1);
pos        = cumsum(cat(1,0,accumarray(mzDistance,1)));
c          = 0;
scanPointNewRoiIndex = zeros(0,1);
for (i = 1:length(count) - 1)
    if (count(i) == 1)
        c                  = c + 1;
        newRoI(c)          = newMZ(pos(i + 1));  %#ok<*AGROW>
        scanIntensities(c,:) = newMS(pos(i + 1),:);
        %         mzBinned(c,:) = newmzBinned(pos(i + 1),:);
    else
        nPts  = count(i) ;
        wL    = newMZ(pos(i) + 1) - mzerror/2:mzerror:newMZ(pos(i + 1)) + mzerror/3;
        wU    = wL + mzerror;
        mzLoc = newMZ(pos(i) + 1:pos(i + 1));
        iLoc  = newMS(pos(i) + 1:pos(i + 1),:);
        iLoc(isnan(iLoc))=0;
        
        %Only include mz's with intensity > threshold. The threshold is
        %calculated as a percentage of the the maximum peak within the scan
        %         iLoc(iLoc)=nan;
        %         mzLoc(iLoc)=nan;
        len   = length(wL);
        [scanPointRoiIndex,nPointsInROI] = basicBin(1,nPts,zeros(nPts,1),zeros(len,1),mzLoc,[],wL,wU);
        if sum(nPointsInROI==0)>0
            len= length(wL)-1;
        else
            len = length(wL);
        end
        if sum(scanPointRoiIndex==0)>0
            d= scanPointRoiIndex(scanPointRoiIndex>0);
            scanPointRoiIndex(length(d):end)=d(end); %%check that this is a valid way of doing it
        end
        newRoI(c + 1:c + len) = accumarray(scanPointRoiIndex,mzLoc) ./  nPointsInROI(1:len);
        
        newRoI(c + 1:c + len) = accumarray(scanPointRoiIndex,mzLoc) ./  nPointsInROI(1:len);
        
        scanPointNewRoiIndex  = cat(1,scanPointNewRoiIndex,scanPointRoiIndex + nroi + c);
        
        if size(iLoc,2)>1
            subs=[];
            for i=1:size(iLoc,1)
                v=1:size(iLoc,2);
                subs=cat(2,subs,v);
            end
            iLocInt=[];
            for i=1:size(iLoc,2)
                iLocInt=cat(1,iLocInt,iLoc(:,i));
            end
            if len>1
                u=unique(scanPointRoiIndex);
                for i=1:len
                    multiply=sum(scanPointRoiIndex==u(i));
                    scanIntensities(c + i,:) = accumarray(subs(1:size(iLoc,2)*multiply)',iLocInt(1:size(iLoc,2)*multiply)')./  nPointsInROI(i); %Calculates average;
                end
            else
                scanIntensities(c + 1:c + len,:) = accumarray(subs',iLocInt)'./  nPointsInROI(1:len); %Calculates average
            end
        else
            scanIntensities(c + 1:c + len,:) = accumarray(scanPointRoiIndex,iLoc) ./  nPointsInROI(1:len); %Calculates average;
        end
        c                              = c + len;
    end
    
    
end
end

