
function[mzroi,MSroi,mzBinned] = CombTwoScans(X,mzerror)
%X: cell array (number of samples , 1) with the mzroi's of the individual
%samples and MSrois of the individual samples
X=ROIsamples;
isample=1;
nSamples=2;
clear mzBinned
MSroi=zeros(size(X{isample}.MSroi,1),size(X{isample}.MSroi,2),length(X));
for isample=1:nSamples
    fprintf(1,'irow: %i\n',isample);
    if isample==1
        mzroi         = X{isample}.mzroi;
        MSroi(:,:,isample)    = X{isample}.MSroi;
        mzBinned(:,:,isample) = X{isample}.mzroi;
        
     
    else
        NewIntLoc=zeros(1,size(X{isample}.MSroi,2));
        PosIntNew=zeros(1,1,1);
        NewLoc=zeros(1,1,1);
        PosNew=zeros(1,1,1);
        PosMZ=zeros(1,1,1);
        intLoc=zeros(1,size(X{isample}.MSroi,2),f);
        mzLoc=zeros(1,1,1);
        
        mzOld=mzroi;
        mzNew  = X{isample}.mzroi;
        IntNew = X{isample}.MSroi;
        
        %Calculates lower and upper boundary of the bin
        wL=mzOld-mzerror/2;
        wU=mzOld+mzerror/2;
        
        %resets counters to 1
        k=1;i=1;s=1;f=1; h=1;d=1;
        while i<=length(mzOld)
            while k <= length(mzNew)&& s<=length(wU)&& i <=length(wU)%&& abs(N(k)-O(i))<=mzerror/2
                if mzNew(k)>wU(s)
                    f           = 1;
                    mzLoc(i,f)  = mzOld(i);
                    PosMZ(i)    = h;
                    intLoc(i,:,f) = 0;
                     h=h+1;
                    i          = i+1;
                    s          = s+1;
                    continue
                   
                end
                if mzNew(k)>=wL(s)
                    f           = f+1;
                    mzLoc(i,f)  = mzNew(k);
                    intLoc(i,:,f) = IntNew(k,:)';   
                    PosMZ(h)    = h;
                else
                    NewLoc(d)    = mzNew(k);
                    NewIntLoc(d,:) = IntNew(k,:);
                    PosNew(i)       = h;
                    h =h+1;
                    d=d+1;
                end
                k=k+1;
            end
         
            if i<=length(mzOld)
                mzLoc(i,1)=mzOld(i);
                intLoc(i,:,f)=0;
                PosMZ(i) = h;
            h=h+1;
            end
            
            %Finds the ROIs in the new mz vector which were not present in the current ROI
            while k<=length(mzNew)
                NewLoc(d)    = mzNew(k);
                NewIntLoc(d,:) = IntNew(k,:);
                PosNew(d)    = h;
                h=h+1;
                d            = d+1;
                k            = k+1;
            end
            i=i+1;
        end
        
        % calculates the mean of the mz value across the two scans and int within
        % the scan
        
        % calculates the mean of the mz value across the two scans and int within
        % the scan
        
        mzLoc(mzLoc==0)=nan;
        intLoc(intLoc==0)=nan;
        intLoc=cat(1,mean(intLoc,3,'omitnan'),NewIntLoc);
          mzroi=[mean(mzLoc,2,'omitnan');NewLoc(NewLoc>0)'];
        dif=length(mzroi)-length(MSroi(:,isample-1));
        
        if dif>=0
            mzBinned=cat(1,mzBinned,nan(dif,size(mzBinned,2)));
            mzBinned=cat(2,mzBinned,[mean(mzLoc,2,'omitnan');NewLoc(NewLoc>0)']);
            MSroi=cat(1,MSroi,zeros(dif,size(MSroi,2),size(MSroi,3)));
            MSroi(:,:,isample)=intLoc;
        end
    end

%     Output
    sortMatrix=sort(cat(2,mzroi,MSroi,mzBinned));
    mzroi=sortMatrix(:,1);
    MSroi=sortMatrix(:,2:size(mzroi,2)+size(MSroi,2));
    mzBinned=sortMatrix(:,size(mzroi,2)+size(MSroi,2)+1:end);
   mzBinned(mzBinned==0)=nan;
end
end